package com.sabre.PushNotificationsApp;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/7/13
 * Time: 10:21 AM
 */
public class Flight {
    private long id;
    private String airlineCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (id != flight.id) return false;
        if (!airlineCode.equals(flight.airlineCode)) return false;
        if (!date.equals(flight.date)) return false;
        if (!flightNumber.equals(flight.flightNumber)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + airlineCode.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + flightNumber.hashCode();
        return result;
    }

    private String date;
    private String flightNumber;
    private String originContext;
    private String originCode;
    private String originGate;
    private String originScheduled;
    private String originScheduledInd;
    private String originTerminal;
    private String destinationContext;
    private String destinationCode;
    private String destinationScheduled;
    private String destinationScheduledInd;
    private String dayInd;
    private long timestamp;

    public Flight() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getOriginContext() {
        return originContext;
    }

    public void setOriginContext(String originContext) {
        this.originContext = originContext;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getOriginGate() {
        return originGate;
    }

    public void setOriginGate(String originGate) {
        this.originGate = originGate;
    }

    public String getOriginScheduled() {
        return originScheduled;
    }

    public void setOriginScheduled(String originScheduled) {
        this.originScheduled = originScheduled;
    }

    public String getOriginScheduledInd() {
        return originScheduledInd;
    }

    public void setOriginScheduledInd(String originScheduledInd) {
        this.originScheduledInd = originScheduledInd;
    }

    public String getOriginTerminal() {
        return originTerminal;
    }

    public void setOriginTerminal(String originTerminal) {
        this.originTerminal = originTerminal;
    }

    public String getDestinationContext() {
        return destinationContext;
    }

    public void setDestinationContext(String destinationContext) {
        this.destinationContext = destinationContext;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getDestinationScheduled() {
        return destinationScheduled;
    }

    public void setDestinationScheduled(String destinationScheduled) {
        this.destinationScheduled = destinationScheduled;
    }

    public String getDestinationScheduledInd() {
        return destinationScheduledInd;
    }

    public void setDestinationScheduledInd(String destinationScheduledInd) {
        this.destinationScheduledInd = destinationScheduledInd;
    }

    public String getDayInd() {
        return dayInd;
    }

    public void setDayInd(String dayInd) {
        this.dayInd = dayInd;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
