package com.sabre.PushNotificationsApp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/7/13
 * Time: 10:44 AM
 */
public class FlightsDataSource {
        private static final String TAG = "FlightsDataSource";

        // Database fields
        private SQLiteDatabase database;
        private FlightsSQLiteHelper dbHelper;
        private String[] allColumns = { FlightsSQLiteHelper.COLUMN_ID,
                FlightsSQLiteHelper.COLUMN_FLIGHT_DATE, FlightsSQLiteHelper.COLUMN_AIRLINE_CODE,
                FlightsSQLiteHelper.COLUMN_FLIGHT_NUMBER,  FlightsSQLiteHelper.COLUMN_O_CONTEXT,
                FlightsSQLiteHelper.COLUMN_O_CODE, FlightsSQLiteHelper.COLUMN_O_GATE,
                FlightsSQLiteHelper.COLUMN_O_SCHEDULED, FlightsSQLiteHelper.COLUMN_O_SCHEDULED_IND,
                FlightsSQLiteHelper.COLUMN_O_TERMINAL, FlightsSQLiteHelper.COLUMN_D_CONTEXT,
                FlightsSQLiteHelper.COLUMN_D_CODE, FlightsSQLiteHelper.COLUMN_D_SCHEDULED,
                FlightsSQLiteHelper.COLUMN_D_SCHEDULED_IND, FlightsSQLiteHelper.COLUMN_DAY_IND,
                FlightsSQLiteHelper.COLUMN_TIMESTAMP};

        public FlightsDataSource(Context context) {
            dbHelper = new FlightsSQLiteHelper(context);
        }

        public void open() throws SQLException {
            database = dbHelper.getWritableDatabase();
        }

        public void close() {
            dbHelper.close();
        }

        public Flight createFlight(Flight flight) {
            Log.i(TAG, "Creating flight id=" + flight.getId());
            ContentValues values = new ContentValues();
            values.put(FlightsSQLiteHelper.COLUMN_ID, flight.getId());
            values.put(FlightsSQLiteHelper.COLUMN_AIRLINE_CODE, flight.getAirlineCode());
            values.put(FlightsSQLiteHelper.COLUMN_FLIGHT_DATE, flight.getDate());
            values.put(FlightsSQLiteHelper.COLUMN_FLIGHT_NUMBER, flight.getFlightNumber());
            values.put(FlightsSQLiteHelper.COLUMN_O_CONTEXT, flight.getOriginContext());
            values.put(FlightsSQLiteHelper.COLUMN_O_CODE, flight.getOriginCode());
            values.put(FlightsSQLiteHelper.COLUMN_O_GATE, flight.getOriginGate());
            values.put(FlightsSQLiteHelper.COLUMN_O_SCHEDULED, flight.getOriginScheduled());
            values.put(FlightsSQLiteHelper.COLUMN_O_SCHEDULED_IND, flight.getOriginScheduledInd());
            values.put(FlightsSQLiteHelper.COLUMN_O_TERMINAL, flight.getOriginTerminal());
            values.put(FlightsSQLiteHelper.COLUMN_D_CONTEXT, flight.getDestinationContext());
            values.put(FlightsSQLiteHelper.COLUMN_D_CODE, flight.getDestinationCode());
            values.put(FlightsSQLiteHelper.COLUMN_D_SCHEDULED ,flight.getDestinationScheduled());
            values.put(FlightsSQLiteHelper.COLUMN_D_SCHEDULED_IND, flight.getDestinationScheduledInd());
            values.put(FlightsSQLiteHelper.COLUMN_DAY_IND, flight.getDayInd());
            values.put(FlightsSQLiteHelper.COLUMN_TIMESTAMP, flight.getTimestamp());

            long insertId = database.insert(FlightsSQLiteHelper.TABLE_FLIGHTS, null, values);
            Cursor cursor = database.query(FlightsSQLiteHelper.TABLE_FLIGHTS,
                    allColumns, FlightsSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                    null, null, null);
            cursor.moveToFirst();
            Flight newFlight = cursorToFlight(cursor);
            cursor.close();
            return newFlight;
        }

        public void deleteFlight(Flight flight) {
            long id = flight.getId();
            System.out.println("Flight deleted with id: " + id);
            database.delete(FlightsSQLiteHelper.TABLE_FLIGHTS, FlightsSQLiteHelper.COLUMN_ID
                    + " = " + id, null);
        }

        public List<Flight> getAllFlights() {
            List<Flight> flights = new ArrayList<Flight>();

            Cursor cursor = database.query(FlightsSQLiteHelper.TABLE_FLIGHTS,
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Flight flight = cursorToFlight(cursor);
                flights.add(flight);
                cursor.moveToNext();
            }
            // Make sure to close the cursor
            cursor.close();
            return flights;
        }

        private Flight cursorToFlight(Cursor cursor) {
            Flight flight = new Flight();
            flight.setId(cursor.getLong(0));
            flight.setDate(cursor.getString(1));
            flight.setAirlineCode(cursor.getString(2));
            flight.setFlightNumber(cursor.getString(3));
            flight.setOriginContext(cursor.getString(4));
            flight.setOriginCode(cursor.getString(5));
            flight.setOriginGate(cursor.getString(6));
            flight.setOriginScheduled(cursor.getString(7));
            flight.setDestinationScheduledInd(cursor.getString(8));
            flight.setOriginTerminal(cursor.getString(9));
            flight.setDestinationContext(cursor.getString(10));
            flight.setDestinationCode(cursor.getString(11));
            flight.setDestinationScheduled(cursor.getString(12));
            flight.setDestinationScheduledInd(cursor.getString(13));
            flight.setDayInd(cursor.getString(14));
            flight.setTimestamp(cursor.getLong(15));
            return flight;
        }

    public void updateFlight(Flight flight) {
        ContentValues values = new ContentValues();
        values.put(FlightsSQLiteHelper.COLUMN_ID, flight.getId());
        values.put(FlightsSQLiteHelper.COLUMN_AIRLINE_CODE, flight.getAirlineCode());
        values.put(FlightsSQLiteHelper.COLUMN_FLIGHT_DATE, flight.getDate());
        values.put(FlightsSQLiteHelper.COLUMN_FLIGHT_NUMBER, flight.getFlightNumber());
        values.put(FlightsSQLiteHelper.COLUMN_O_CONTEXT, flight.getOriginContext());
        values.put(FlightsSQLiteHelper.COLUMN_O_CODE, flight.getOriginCode());
        values.put(FlightsSQLiteHelper.COLUMN_O_GATE, flight.getOriginGate());
        values.put(FlightsSQLiteHelper.COLUMN_O_SCHEDULED, flight.getOriginScheduled());
        values.put(FlightsSQLiteHelper.COLUMN_O_SCHEDULED_IND, flight.getOriginScheduledInd());
        values.put(FlightsSQLiteHelper.COLUMN_O_TERMINAL, flight.getOriginTerminal());
        values.put(FlightsSQLiteHelper.COLUMN_D_CONTEXT, flight.getDestinationContext());
        values.put(FlightsSQLiteHelper.COLUMN_D_CODE, flight.getDestinationCode());
        values.put(FlightsSQLiteHelper.COLUMN_D_SCHEDULED ,flight.getDestinationScheduled());
        values.put(FlightsSQLiteHelper.COLUMN_D_SCHEDULED_IND, flight.getDestinationScheduledInd());
        values.put(FlightsSQLiteHelper.COLUMN_DAY_IND, flight.getDayInd());
        values.put(FlightsSQLiteHelper.COLUMN_TIMESTAMP, flight.getTimestamp());

        String whereClause = "id=?";
        String[] whereArgs = {String.valueOf(flight.getId())};

        int numRows = database.update(FlightsSQLiteHelper.TABLE_FLIGHTS, values, whereClause, whereArgs);
        Log.i(TAG, "Updated " + numRows + " row in database");
    }
}
