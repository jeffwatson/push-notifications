/**
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/20/2013
 * Time: 10:20 AM
 *
 * This application will receive push notifications from
 * the corresponding web app.
 */
package com.sabre.PushNotificationsApp;

import android.app.*;
import android.content.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import org.json.JSONObject;


import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class FlightsFragment extends android.support.v4.app.Fragment {//implements AdapterView.OnItemClickListener{
    // log tag for this activity
    private static final String TAG = "FlightsFragment";

    public static final String JSON_PAYLOAD_RECEIVED = "com.sabre.PushNotificationsApp.JSONPayload";
    public static final String RECEIVED_MESSAGE = "com.sabre.PushNotificationsApp.SimpleMessage";

    public static final String EXTRA_MESSAGE = "message";

    private static final String UPDATE_FLIGHTS_URL = "http://wl16-int.sabresonicweb.com/PushNotificationsWeb_war_exploded/flight_picker?";


    // the context of this application
    private static Context context;

    private static FlightsDataSource datasource;
    private static WeatherDataSource wDatasource;
    private static List<Flight> flightList;
    private static List<Weather> weatherList;
    private static ListView mListView;
    private static FlightsListAdapter mAdapter;
    private static Activity parentActivity;


    /**
     * Called when the activity is first created.
     *
     * TODO look for Google source app... make this work nicer.
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = getActivity();
        this.context = inflater.getContext();

        datasource = new FlightsDataSource(context);
        wDatasource = new WeatherDataSource(context);

        // get our list of flights
        datasource.open();
        flightList = datasource.getAllFlights();
        datasource.close();

        wDatasource.open();
        weatherList = wDatasource.getAllWeather();
        wDatasource.close();


        View mainLayout = inflater.inflate(R.layout.fragment_flights_list, container, false);


        // set the click listener for this button instead of the hacky xml way
        Button registrationButton = (Button) mainLayout.findViewById(R.id.btn_flight_register);
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBookFlightDialog();
            }
        });


        mListView = (ListView) mainLayout.findViewById(R.id.flights_list);

        mAdapter = new FlightsListAdapter(context, flightList, weatherList);
        mListView.setAdapter(mAdapter);

        //mListView.setOnItemClickListener(this);

        return mainLayout;

        // TODO need to process incoming JSON and update UI

        /*Log.i(TAG, "Got "+ flightList.size() + " flights from the database.");
        ViewGroup fragment_flights_list = (ViewGroup) findViewById(R.id.fragment_flights_list);
        //SimpleDateFormat fromFormat = new SimpleDateFormat("k:m:s");
        SimpleDateFormat toFormat = new SimpleDateFormat("h:mm a");
        ListView mListView = (ListView) findViewById(R.id.flights_list);

        for(Flight f: flightList) {
            View layout = View.inflate(this, R.layout.flight_item, null);

            //mListView.

            // set the tag of 0 to the id so we can access these bad boys later if the data changes
            //layout.setTag(0, f.getId());

            // set all the values of this layout
            TextView mTv = (TextView) layout.findViewById(R.id.flight_number);
            mTv.setText(f.getAirlineCode() + f.getFlightNumber()); // concat airline code and flight number for clarity

            mTv = (TextView) layout.findViewById(R.id.departs_code_value);
            mTv.setText(f.getOriginCode()); // TODO translate codes to names i.e. DFW = Dallas

            mTv = (TextView) layout.findViewById(R.id.departure_value);
            mTv.setText(f.getOriginScheduled());

            mTv = (TextView) layout.findViewById(R.id.arrival_value);
            mTv.setText(f.getDestinationScheduled());

            mTv = (TextView) layout.findViewById(R.id.last_updated_value);
            mTv.setText(toFormat.format(f.getTimestamp()));

            mTv = (TextView) layout.findViewById(R.id.arrives_code_value);
            mTv.setText(f.getDestinationCode()); // TODO translate code to name...

            mTv = (TextView) layout.findViewById(R.id.time_delta_in);
            mTv.setText(getTimeDelta(f.getDate(),f.getOriginScheduled()));
        }                   */




        /*String action = this.getIntent().getAction();
        if(action != null && action.equals(JSON_PAYLOAD_RECEIVED))
        {
            String JSON = this.getIntent().getStringExtra("JSONString");
            Toast.makeText(this, JSON, Toast.LENGTH_SHORT).show();

            SimpleDateFormat fromFormat = new SimpleDateFormat("k:m:s");
            SimpleDateFormat toFormat = new SimpleDateFormat("h:mm a");

            try {
                JSONObject mObject = new JSONObject(JSON);
                TextView flightNumber = (TextView) findViewById(R.id.flight_number);
                String oldText = (String) flightNumber.getText();
                String newText = mObject.getString("airline_code") + mObject.getString("flight_number");
                if(!oldText.equals(newText)) { flightNumber.setBackgroundResource(R.color.red); }
                flightNumber.setText(newText);

                TextView departsFrom = (TextView) findViewById(R.id.departs_code_value);
                oldText = (String) departsFrom.getText();
                newText = mObject.getString("o_code");
                if(!oldText.equals(newText)) { departsFrom.setBackgroundResource(R.color.red); }
                departsFrom.setText(newText);

                TextView arrivesAt = (TextView) findViewById(R.id.arrives_code_value);
                oldText = (String) arrivesAt.getText();
                newText = mObject.getString("d_code");
                if(!oldText.equals(newText)) { arrivesAt.setBackgroundResource(R.color.red); }
                arrivesAt.setText(newText);

                TextView departureValue = (TextView) findViewById(R.id.departure_value);
                oldText = (String) departureValue.getText();
                newText = toFormat.format(fromFormat.parse(mObject.getString("o_scheduled")));
                if(!oldText.equals(newText)) { departureValue.setBackgroundResource(R.color.red); }
                departureValue.setText(newText);

                TextView arrivalValue = (TextView) findViewById(R.id.arrival_value);
                oldText = (String) arrivalValue.getText();
                newText = toFormat.format(fromFormat.parse(mObject.getString("d_scheduled")));
                if(!oldText.equals(newText)) { arrivalValue.setBackgroundResource(R.color.red); }
                arrivalValue.setText(newText);

                TextView lastUpdated = (TextView) findViewById(R.id.last_updated_value);
                lastUpdated.setText(toFormat.format(System.currentTimeMillis()));

                TextView timeDelta = (TextView) findViewById(R.id.time_delta_in);
                timeDelta.setText(getTimeDelta((String)mObject.get("flight_date"),(String)mObject.get("o_scheduled")));

            } catch (JSONException e) {
                Log.e(TAG, "Unable to parse JSON.", e);
            }  catch (ParseException e) {
                Log.e(TAG, "Unable to parse time :(", e);
            }

        }  */
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO connect to server and ask (politely) for updated data
        for(Flight f: flightList) {
            updateFlightData(f, mListView, flightList.indexOf(f));
        }
    }



    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     *  context application's context.
     *  message message to be displayed.
     */
    /*static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }  */

    public void showBookFlightDialog() {
       FragmentManager fm = getActivity().getSupportFragmentManager();
       WatchFlightDialogFragment watchDialog = new WatchFlightDialogFragment();

        watchDialog.show(fm, "fragment_watch");
    }

    //TODO add an animation when this successfully adds a flight to the list
    public static void addFlight(final Flight f) {
        flightList.add(f);
        final int index = flightList.indexOf(f);

        scrollToPosition(index);
    }

    public static void updateFlight(final Flight f) {
        updateFlightList();
        final int index = flightList.indexOf(f);
        Log.i(TAG, "Updating flight view #"+index);

        scrollToPosition(index);
    }

    public static void scrollToPosition(final int position) {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mListView.setSelection(position);
                mListView.smoothScrollToPosition(position);
            }
        });

    }

    public static void updateFlightList(){
        datasource.open();
        flightList = datasource.getAllFlights();
        datasource.close();
    }


    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the fragment_registration id, app versionCode, and expiration time in the
     * application's shared preferences.
     */
    @SuppressWarnings("unchecked")
    private void updateFlightData(final Flight f, final ListView lv, final int position) {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                    // if (gcm == null) {
                    //   gcm = GoogleCloudMessaging.getInstance(context);
                    // }
                    //regid = gcm.register(SENDER_ID);
                    //msg = "Device registered, fragment_registration id=" + regid;
                    //readPrefs();

                    // You should send the fragment_registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.

                    HttpURLConnection connection;
                    OutputStreamWriter request = null;

                    URL url;// = null;
                    final String response;// = null;
                    final String parameters = "flight="+ f.getFlightNumber() +"&airline="+ f.getAirlineCode() + "&date=" + f.getDate();

                    url = new URL(UPDATE_FLIGHTS_URL + parameters);
                    connection = (HttpURLConnection) url.openConnection();
                    //connection.setDoOutput(true);
                    //connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestMethod("GET");

                    //request = new OutputStreamWriter(connection.getOutputStream());
                    //request.write(parameters);
                    //request.flush();
                    //request.close();
                    String line = "";
                    InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(isr);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    // Response from server after login process will be stored in response variable.
                    response = sb.toString();
                    Log.i(TAG, response);
                    // TODO update the UI with lv
                    final HashMap<String, Object> objects = stringToFlightAndWeather(response);
                    // You can perform UI operations here
                    parentActivity.runOnUiThread(new Runnable(){

                        // TODO this needs to highlight the data that changed.
                        @Override
                        public void run() {
                            Toast.makeText(context, "Message to server: " + parameters + "\nMessage from Server: \n" + response, Toast.LENGTH_SHORT).show();


                            //try {
                            //Flight f = (Flight) objects.get("flight");
                            //Weather oWeather = (Weather) objects.get("oWeather");
                            //Weather dWeather = (Weather) objects.get("dWeather");

                            //View v = (View) lv.getChildAt(position);
                            //TextView mTv = (TextView) v.findViewById(R.id.arrival_time);
                            //String oldText = mTv.getText().toString();
                            //if(!oldText.equals(f.getDestinationScheduled()))
                            //{
                            //    mTv.setBackgroundColor(R.color.red);
                            //}
                            //mTv.setText(f.getDestinationScheduled());
                            //} catch (Exception e) {
                            //    Log.i(TAG, "Error updating in Asynctask.", e);
                            //}
                        }
                    });
                    //Toast.makeText(context, "Message from Server: \n" + response, Toast.LENGTH_SHORT).show();
                    isr.close();
                    reader.close();

                    // Save the regid - no need to register again.
                    //setRegistrationId(ctx, regid);
                } catch (IOException ex) {
                    msg = "Error: " + ex.toString();
                    Log.e(TAG, "Error updating flight info.", ex);
                    //System.out.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(Object o) {
                //mDisplay.append(o.toString() + "\n");
            }

        }.execute(null, null, null);
    }

    private HashMap<String, Object> stringToFlightAndWeather(String json){
        HashMap<String, Object> info = new HashMap<String, Object>();

        try {
            JSONObject mObject = new JSONObject(json);
            Flight newFlight = new Flight();
            Weather oWeather = new Weather();
            Weather dWeather = new Weather();

            newFlight.setId(mObject.getLong("id"));
            newFlight.setAirlineCode(mObject.getString("airline_code"));
            newFlight.setDate(mObject.getString("flight_date"));
            newFlight.setFlightNumber(mObject.getString("flight_number"));
            newFlight.setOriginContext(mObject.getString("o_context"));
            newFlight.setOriginCode(mObject.getString("o_code"));
            newFlight.setOriginGate(mObject.getString("o_gate"));
            newFlight.setOriginScheduled(mObject.getString("o_scheduled"));//toFormat.format(fromFormat.parse(mObject.getString("o_scheduled")))); // TODO
            newFlight.setOriginScheduledInd(mObject.getString("o_scheduled_ind"));
            newFlight.setOriginTerminal(mObject.getString("o_terminal"));
            newFlight.setDestinationContext(mObject.getString("d_context"));
            newFlight.setDestinationCode(mObject.getString("d_code"));
            newFlight.setDestinationScheduled(mObject.getString("d_scheduled"));//toFormat.format(fromFormat.parse(mObject.getString("d_scheduled")))); // TODO
            newFlight.setDestinationScheduledInd(mObject.getString("d_scheduled_ind"));
            newFlight.setDayInd(mObject.getString("day_ind"));
            newFlight.setTimestamp(System.currentTimeMillis());

            oWeather.setCity(mObject.getString("o_city_name"));
            oWeather.setCode(mObject.getString("o_code"));
            oWeather.setMinTemp(mObject.getString("o_min_temp"));
            oWeather.setMaxTemp(mObject.getString("o_max_temp"));
            oWeather.setCondition(mObject.getString("o_condition"));

            dWeather.setCity(mObject.getString("d_city_name"));
            dWeather.setCode(mObject.getString("d_code"));
            dWeather.setMinTemp(mObject.getString("d_min_temp"));
            dWeather.setMaxTemp(mObject.getString("d_max_temp"));
            dWeather.setCondition(mObject.getString("d_condition"));

            info.put("flight", newFlight);
            info.put("oWeather", oWeather);
            info.put("dWeather", dWeather);

            wDatasource.open();

            int numRows = wDatasource.updateWeather(oWeather);
            if(numRows == 0) {
                wDatasource.createWeather(oWeather);
            }

            numRows = wDatasource.updateWeather(dWeather);
            if(numRows == 0) {
                wDatasource.createWeather(dWeather);
            }

            wDatasource.close();

            //TODO update DBs here
        } catch (Exception e) {
            Log.e(TAG, "Error converting JSON to Objects.", e);
        }

        return info;
    }
}
