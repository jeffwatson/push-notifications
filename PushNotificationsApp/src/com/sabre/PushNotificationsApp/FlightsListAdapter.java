package com.sabre.PushNotificationsApp;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/8/13
 * Time: 9:56 AM
 */
public class FlightsListAdapter extends BaseAdapter{

    private static final String TAG = "FlightsListAdapter";

    private final Context context;
    private List<Flight> mList;
    private List<Weather> mWeatherList;

    public FlightsListAdapter(Context context, List<Flight> list, List<Weather> weathers) {
        super();
        this.context = context;
        this.mList = list;
        this.mWeatherList = weathers;
    }

    // TODO add the "WAS" field -> need new database :(
    // TODO convert 3 letter code to a city.

    // TODO add weather icons and data
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i(TAG, "Getting view "+ position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");

        View layout = inflater.inflate(R.layout.flight_item, parent, false);

        Flight f = mList.get(position);

        Weather origin = null, destination = null;



        String value;

        // set all the values of this layout
        TextView mTv = (TextView) layout.findViewById(R.id.flight_number);
        mTv.setText(f.getAirlineCode() + f.getFlightNumber()); // concat airline code and flight number for clarity

        mTv = (TextView) layout.findViewById(R.id.departs_from);
        value = String.format(context.getResources().getString(R.string.departs_from), f.getOriginCode());
        mTv.setText(value); // TODO translate codes to names i.e. DFW = Dallas



        String AMPM = f.getOriginScheduled().substring(f.getOriginScheduled().indexOf(" ") + 1, f.getOriginScheduled().length());
        String timeValue = f.getOriginScheduled().substring(0, f.getOriginScheduled().indexOf(" "));
        mTv = (TextView) layout.findViewById(R.id.departure_value);
        mTv.setText(timeValue);

        mTv = (TextView) layout.findViewById(R.id.departure_AMPM);
        mTv.setText(AMPM);

        AMPM = f.getDestinationScheduled().substring(f.getDestinationScheduled().indexOf(" ") + 1, f.getDestinationScheduled().length());
        timeValue = f.getDestinationScheduled().substring(0, f.getDestinationScheduled().indexOf(" "));
        mTv = (TextView) layout.findViewById(R.id.arrival_value);
        mTv.setText(timeValue);

        mTv = (TextView) layout.findViewById(R.id.arrival_AMPM);
        mTv.setText(AMPM);

        mTv = (TextView) layout.findViewById(R.id.last_updated);
        value = String.format(context.getResources().getString(R.string.last_updated), toFormat.format(f.getTimestamp()));
        mTv.setText(value);



        mTv = (TextView) layout.findViewById(R.id.arrives_at);
        value = String.format(context.getResources().getString(R.string.arrives_at), f.getDestinationCode());
        mTv.setText(value); // TODO translate code to name...

        mTv = (TextView) layout.findViewById(R.id.time_delta_in);
        mTv.setText(getTimeDelta(f.getDate(),f.getOriginScheduled()));



        try {
            //create a weather to find these values in the list
            Weather w = new Weather();
            w.setCode(f.getOriginCode());


            origin = mWeatherList.get(mWeatherList.indexOf(w));

            w.setCode(f.getDestinationCode());
            destination = mWeatherList.get(mWeatherList.indexOf(w));

            mTv = (TextView) layout.findViewById(R.id.arrives_weather);
            value = String.format(context.getResources().getString(R.string.temperatures), destination.getMaxTemp(), destination.getMinTemp());
            mTv.setText(value);

            mTv = (TextView) layout.findViewById(R.id.departs_weather);
            value = String.format(context.getResources().getString(R.string.temperatures), origin.getMaxTemp(), origin.getMinTemp());
            mTv.setText(value);

            ImageView iv = (ImageView) layout.findViewById(R.id.departs_img);
            setWeatherDrawable(iv, origin.getCondition());

            iv = (ImageView) layout.findViewById(R.id.arrives_img);
            setWeatherDrawable(iv, destination.getCondition());
        } catch(Exception e) {
            Log.e(TAG, "Unable to get weather information", e);
        }

        return layout;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int position) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private String getTimeDelta(String flight_date, String o_scheduled) {
        String timeString;
        long value;

        Resources res = context.getResources();
        long dateAsLong = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a");

        try {
            dateAsLong = sdf.parse(flight_date +" " + o_scheduled).getTime();
        } catch (ParseException e) {
            Log.e(TAG, "Unable to parse date for delta.", e);
        }

        long timeDelta = dateAsLong - System.currentTimeMillis();
        if(Math.abs(timeDelta) > 86400000) {
            // show days
            value = TimeUnit.DAYS.convert(timeDelta, TimeUnit.MILLISECONDS);// Days
            timeString = res.getQuantityString(R.plurals.days, safeLongToInt(value), value);
        } else if (Math.abs(timeDelta) > 3600000) {
            //show hours
            value = TimeUnit.HOURS.convert(timeDelta, TimeUnit.MILLISECONDS);
            timeString = res.getQuantityString(R.plurals.hours, safeLongToInt(value), value);
        } else {
            //show minutes
            value = TimeUnit.MINUTES.convert(timeDelta, TimeUnit.MILLISECONDS);
            timeString = res.getQuantityString(R.plurals.minutes, safeLongToInt(value), value);
        }

        return timeString;
    }

    private void setWeatherDrawable(ImageView iv, String condition) {
        if(condition.equals("01d")){
            iv.setImageResource(R.drawable.day_clear);
        } else if (condition.equals("02d")) {
            iv.setImageResource(R.drawable.day_few_clouds);
        } else if (condition.equals("03d")) {
            iv.setImageResource(R.drawable.day_scattered_clouds);
        } else if (condition.equals("04d")) {
            iv.setImageResource(R.drawable.day_broken_clouds);
        } else if (condition.equals("09d")) {
            iv.setImageResource(R.drawable.day_shower);
        } else if (condition.equals("10d")) {
            iv.setImageResource(R.drawable.day_rain);
        } else if (condition.equals("11d")) {
            iv.setImageResource(R.drawable.day_thunderstorm);
        } else if (condition.equals("13d")) {
            iv.setImageResource(R.drawable.day_snow);
        } else if (condition.equals("50d")) {
            iv.setImageResource(R.drawable.day_mist);
        } else if (condition.equals("01n")) {
            iv.setImageResource(R.drawable.night_clear);
        } else if (condition.equals("02n")) {
            iv.setImageResource(R.drawable.night_few_clouds);
        } else if (condition.equals("03n")) {
            iv.setImageResource(R.drawable.night_scattered_clouds);
        } else if (condition.equals("04n")) {
            iv.setImageResource(R.drawable.night_broken_clouds);
        } else if (condition.equals("09n")) {
            iv.setImageResource(R.drawable.night_shower);
        } else if (condition.equals("10n")) {
            iv.setImageResource(R.drawable.night_rain);
        } else if (condition.equals("11n")) {
            iv.setImageResource(R.drawable.night_thunderstorm);
        } else if (condition.equals("13n")) {
            iv.setImageResource(R.drawable.night_snow);
        } else if (condition.equals("50n")){
            iv.setImageResource(R.drawable.night_mist);
        }

    }

    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }
}
