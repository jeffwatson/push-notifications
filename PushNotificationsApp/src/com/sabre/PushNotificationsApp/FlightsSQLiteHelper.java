package com.sabre.PushNotificationsApp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/7/13
 * Time: 10:25 AM
 */
public class FlightsSQLiteHelper extends SQLiteOpenHelper{


    public static final String TABLE_FLIGHTS = "flights";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FLIGHT_DATE = "flight_date";
    public static final String COLUMN_AIRLINE_CODE = "airline_code";
    public static final String COLUMN_FLIGHT_NUMBER = "flight_number";
    public static final String COLUMN_O_CONTEXT = "o_context";
    public static final String COLUMN_O_CODE = "o_code";
    public static final String COLUMN_O_GATE = "o_gate";
    public static final String COLUMN_O_SCHEDULED = "o_scheduled";
    public static final String COLUMN_O_SCHEDULED_IND = "o_scheduled_ind";
    public static final String COLUMN_O_TERMINAL = "o_terminal";
    public static final String COLUMN_D_CONTEXT = "d_context";
    public static final String COLUMN_D_CODE = "d_code";
    public static final String COLUMN_D_SCHEDULED = "d_scheduled";
    public static final String COLUMN_D_SCHEDULED_IND = "d_scheduled_ind";
    public static final String COLUMN_DAY_IND = "day_ind";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    private static final String DATABASE_NAME = "flights.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_FLIGHTS + "(" + COLUMN_ID
            + " integer primary key, " + COLUMN_FLIGHT_DATE
            + " text not null, " + COLUMN_AIRLINE_CODE + " text not null, " + COLUMN_FLIGHT_NUMBER
            + " text not null, " + COLUMN_O_CONTEXT + " text not null, " + COLUMN_O_CODE
            + " text not null, " + COLUMN_O_GATE + " text not null, " + COLUMN_O_SCHEDULED
            + " text not null, " + COLUMN_O_SCHEDULED_IND + " text not null, " + COLUMN_O_TERMINAL
            + " text not null, " + COLUMN_D_CONTEXT + "  text not null, " + COLUMN_D_CODE
            + " text not null, " + COLUMN_D_SCHEDULED + " text not null, " + COLUMN_D_SCHEDULED_IND
            + " text not null, " + COLUMN_DAY_IND + "  text not null, " + COLUMN_TIMESTAMP
            + " text not null);";

    public FlightsSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(FlightsSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FLIGHTS);
        onCreate(db);
    }

}

