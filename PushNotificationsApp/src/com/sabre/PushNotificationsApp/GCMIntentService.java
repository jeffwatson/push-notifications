package com.sabre.PushNotificationsApp;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/21/13
 * Time: 11:31 AM
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import com.google.android.gcm.GCMBaseIntentService;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Handling of GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {
    static final String TAG = "PushBroadcastReceiver";
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    private static Context ctx;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String API_KEY = "AIzaSyCG0znP4YAMbruFvZi7LMR6fIyXCa6I1OA";

    private static final String REGISTRATION_URL = "https://wl16-int.sabresonicweb.com/PushNotificationsWeb_war_exploded/registration";

    private static String username;
    private static String regid;


    /**
     * Default lifespan (7 days) of a reservation until it is considered expired.
     */
    public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;

    public GCMIntentService() {
        super(API_KEY);
        ctx = this;
    }

    // Put the GCM message into a notification and post it.
    private void sendNotification(String title, String subtitle, String data) {
        mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;
        Intent messageDataIntent;

        Log.i(TAG, "subtitle= " + subtitle);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_plane_gold)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(subtitle))
                        .setContentText(subtitle)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL);

        if(data != null) {
            // Include the JSON message in the notification
            messageDataIntent = new Intent(FlightsFragment.JSON_PAYLOAD_RECEIVED, null, ctx, PushNotificationsFragmentActivity.class);
            messageDataIntent.putExtra("JSONString", data);
            contentIntent = PendingIntent.getActivity(ctx, 0, messageDataIntent , 0);


        } else {
            messageDataIntent = new Intent(FlightsFragment.RECEIVED_MESSAGE, null, ctx, PushNotificationsFragmentActivity.class);
            contentIntent = PendingIntent.getActivity(ctx, 0, messageDataIntent, 0);
        }

        mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    @Override
    protected void onMessage(Context context, Intent intent) {

        String action = intent.getStringExtra("action");
        String title = intent.getStringExtra("title");
        String subtitle = intent.getStringExtra("subtitle");
        String data = intent.getStringExtra("data");

        Log.i(TAG, "Action: " + action + ", Title: " + title + ", Subtitle: " + subtitle + ", data: " + data);

        if(action.equals("flight") && data != null) {

            SimpleDateFormat fromFormat = new SimpleDateFormat("k:m:s");
            SimpleDateFormat toFormat = new SimpleDateFormat("h:mm a");

            Flight newFlight = new Flight();
            try {
                JSONObject mObject = new JSONObject(data);
                newFlight.setId(mObject.getLong("id"));
                newFlight.setAirlineCode(mObject.getString("airline_code"));
                newFlight.setDate(mObject.getString("flight_date"));
                newFlight.setFlightNumber(mObject.getString("flight_number"));
                newFlight.setOriginContext(mObject.getString("o_context"));
                newFlight.setOriginCode(mObject.getString("o_code"));
                newFlight.setOriginGate(mObject.getString("o_gate"));
                newFlight.setOriginScheduled(toFormat.format(fromFormat.parse(mObject.getString("o_scheduled"))));
                newFlight.setOriginScheduledInd(mObject.getString("o_scheduled_ind"));
                newFlight.setOriginTerminal(mObject.getString("o_terminal"));
                newFlight.setDestinationContext(mObject.getString("d_context"));
                newFlight.setDestinationCode(mObject.getString("d_code"));
                newFlight.setDestinationScheduled(toFormat.format(fromFormat.parse(mObject.getString("d_scheduled"))));
                newFlight.setDestinationScheduledInd(mObject.getString("d_scheduled_ind"));
                newFlight.setDayInd(mObject.getString("day_ind"));
                newFlight.setTimestamp(System.currentTimeMillis());

                try {
                Weather oWeather = new Weather();
                Weather dWeather = new Weather();
                oWeather.setCity(mObject.getString("o_city_name"));
                oWeather.setCode(mObject.getString("o_code"));
                oWeather.setMinTemp(mObject.getString("o_min_temp"));
                oWeather.setMaxTemp(mObject.getString("o_max_temp"));
                oWeather.setCondition(mObject.getString("o_condition"));

                dWeather.setCity(mObject.getString("d_city_name"));
                dWeather.setCode(mObject.getString("d_code"));
                dWeather.setMinTemp(mObject.getString("d_min_temp"));
                dWeather.setMaxTemp(mObject.getString("d_max_temp"));
                dWeather.setCondition(mObject.getString("d_condition"));

                WeatherDataSource wDatasource = new WeatherDataSource(this);
                wDatasource.open();

                int numRows = wDatasource.updateWeather(oWeather);
                if(numRows == 0) {
                    wDatasource.createWeather(oWeather);
                }

                numRows = wDatasource.updateWeather(dWeather);
                if(numRows == 0) {
                    wDatasource.createWeather(dWeather);
                }

                wDatasource.close();
                } catch (Exception e) {
                    Log.e(TAG, "Weather values not found..", e);
                }


                // add this flight to the database
                // TODO there's got to be a better way to do this.
                FlightsDataSource datasource = new FlightsDataSource(context);
                datasource.open();
                try {
                    datasource.createFlight(newFlight);   // this works if the item does not exist yet, otherwise it fails.
                    datasource.close();

                    FlightsFragment.addFlight(newFlight); // update the UI if the app is open.
                } catch (CursorIndexOutOfBoundsException e) {
                    Log.i(TAG, "Unable to CREATE Flight in Service, Attempting to UPDATE.", e);

                    datasource.updateFlight(newFlight);
                    datasource.close();

                    FlightsFragment.updateFlight(newFlight);
                }
                //Log.i(TAG, "GOT DATA: " + datasource.getAllFlights());


            } catch (JSONException e) {
                Log.e(TAG, "Unable to parse JSON in Service", e);
            } catch (ParseException e) {
                Log.e(TAG, "Error parsing to date in Service.", e);
            }

        }

        sendNotification(title, subtitle, data);

        //FlightsFragment.displayMessage(context, data);
    }

    @Override
    protected void onError(Context context, String s) {
        //To change body of implemented methods use File | Settings | File Templates.
        Log.i(TAG, "Error!");
    }

    @Override
    protected void onRegistered(Context context, String s) {
        //To change body of implemented methods use File | Settings | File Templates.
        Log.i(TAG, "Registered! " + s);
        registerBackground(s);
    }

    @Override
    protected void onUnregistered(Context context, String s) {
        //To change body of implemented methods use File | Settings | File Templates.
        Log.i(TAG, "Unregistered!");
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the fragment_registration id, app versionCode, and expiration time in the
     * application's shared preferences.
     */
    @SuppressWarnings("unchecked")
    private void registerBackground(final String regid) {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                   // if (gcm == null) {
                     //   gcm = GoogleCloudMessaging.getInstance(context);
                   // }
                    //regid = gcm.register(SENDER_ID);
                    msg = "Device registered, fragment_registration id=" + regid;
                    readPrefs();

                    // You should send the fragment_registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.

                    HttpsURLConnection connection;
                    OutputStreamWriter request = null;

                    URL url;// = null;
                    String response = null;
                    String parameters = "user="+ username +"&reg_id="+ regid;

                    url = new URL(REGISTRATION_URL);
                    connection = (HttpsURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestMethod("POST");

                    request = new OutputStreamWriter(connection.getOutputStream());
                    request.write(parameters);
                    request.flush();
                    request.close();
                    String line = "";
                    InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(isr);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    // Response from server after login process will be stored in response variable.
                    response = sb.toString();
                    //Log.i(TAG, response);
                    // You can perform UI operations here
                    //Toast.makeText(context, "Message from Server: \n" + response, 0).show();
                    isr.close();
                    reader.close();

                    // Save the regid - no need to register again.
                    setRegistrationId(ctx, regid);
                } catch (IOException ex) {
                    msg = "Error: " + ex.toString();
                    Log.e(TAG, "Error registering", ex);
                    //System.out.
                }
                return msg;
            }

            /*@Override
            protected void onPostExecute(Object o) {
                mDisplay.append(o.toString() + "\n");
            }   */

        }.execute(null, null, null);
    }

    /**
     * Stores the fragment_registration id, app versionCode, and expiration time in the
     * application's {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId fragment_registration id
     */
    private void setRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getSharedPreferences(FlightsFragment.class.getSimpleName(),Context.MODE_PRIVATE);
        int appVersion = getAppVersion(context);
        Log.v(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putString(PROPERTY_USERNAME, username);
        long expirationTime = System.currentTimeMillis() + REGISTRATION_EXPIRY_TIME_MS;

        Log.v(TAG, "Setting fragment_registration expiry time to " +
                new Timestamp(expirationTime));
        editor.putLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
        editor.commit();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void readPrefs() {
        SharedPreferences prefs = getSharedPreferences(FlightsFragment.class.getSimpleName(),Context.MODE_PRIVATE);
        //SharedPreferences.Editor editor = prefs.edit();
        //prefs.edit()
        username = prefs.getString(PROPERTY_USERNAME, "");
        regid = prefs.getString(PROPERTY_REG_ID, "");
    }




}
