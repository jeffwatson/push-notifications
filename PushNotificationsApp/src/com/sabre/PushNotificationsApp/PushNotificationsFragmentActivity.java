package com.sabre.PushNotificationsApp;



import java.sql.Timestamp;
import java.util.Locale;

import android.app.*;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gcm.GCMRegistrar;


/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/8/13
 * Time: 1:28 PM
 */

public class PushNotificationsFragmentActivity extends FragmentActivity implements RegistrationDialogFragment.RegistrationDialogListener {
    private static final String TAG = "PushNotificationsFragmentActivity";
    private static final String DISPLAY_MESSAGE_ACTION = "com.sabre.PushNotificationsApp.DISPLAY_MESSAGE";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTimeMs";

    /**
     * Substitute you own sender ID here.
     */
    String SENDER_ID = "449171388435";

    /**
     * TODO implement a way to refresh the registration id
     * Default lifespan (7 days) of a reservation until it is considered expired.
     */
    public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    String regid;
    String username;
    public static TextView mDisplay;

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;



        Log.i(TAG, "Opening app!");


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // set flights to be the default item
        mViewPager.setCurrentItem(1);

        this.registerReceiver(mHandleMessageReceiver,  new IntentFilter(DISPLAY_MESSAGE_ACTION));

        // keep this it'll throw when the device doesn't have Play Store
        GCMRegistrar.checkDevice(this);

        // this can go, Manifest is fine.
        GCMRegistrar.checkManifest(this);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        this.unregisterReceiver(mHandleMessageReceiver);
    }

    public void showRegistrationDialog() {

        FragmentManager fm = getSupportFragmentManager();
        RegistrationDialogFragment regDialog = new RegistrationDialogFragment();

        regDialog.show(fm, "fragment_registration");
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getPreferences(Context context) {
        return getSharedPreferences(FlightsFragment.class.getSimpleName(),  Context.MODE_PRIVATE);
    }




    /**
     * Gets the current fragment_registration id for application on GCM service.
     * <p>
     * If result is empty, the fragment_registration has failed.
     *
     * @return fragment_registration id, or empty string if the fragment_registration is not
     *         complete.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.length() == 0) {
            Log.v(TAG, "Registration not found.");
            return "";
        }
        // check if app was updated; if so, it must clear fragment_registration id to
        // avoid a race condition if GCM sends a message
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion || isRegistrationExpired()) {
            Log.v(TAG, "App version changed or fragment_registration expired.");
            return "";
        }
        return registrationId;
    }



    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Checks if the fragment_registration has expired.
     *
     * <p>To avoid the scenario where the device sends the fragment_registration to the
     * server but the server loses it, the app developer may choose to re-register
     * after REGISTRATION_EXPIRY_TIME_MS.
     *
     * @return true if the fragment_registration has expired.
     */
    private boolean isRegistrationExpired() {
        final SharedPreferences prefs = getPreferences(context);
        // checks if the information is not stale
        long expirationTime =
                prefs.getLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, -1);
        return System.currentTimeMillis() > expirationTime;
    }

    /**
     * Stores the fragment_registration id, app versionCode, username and expiration time in the
     * application's {@code SharedPreferences}.
     *
     */
    private void savePrefs() {
        final SharedPreferences prefs = getPreferences(context);
        int appVersion = getAppVersion(context);
        Log.v(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putString(PROPERTY_USERNAME, username);
        long expirationTime = System.currentTimeMillis() + REGISTRATION_EXPIRY_TIME_MS;

        Log.v(TAG, "Setting fragment_registration expiry time to " +
                new Timestamp(expirationTime));
        editor.putLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
        editor.commit();
    }

    private void readPrefs() {
        SharedPreferences prefs = getSharedPreferences(FlightsFragment.class.getSimpleName(),Context.MODE_PRIVATE);
        username = prefs.getString(PROPERTY_USERNAME, "");
        regid = prefs.getString(PROPERTY_REG_ID, "");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.register:
                showRegistrationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // TODO implement these
    private final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
                    //mDisplay.append(newMessage + "\n");
                }
            };


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //Fragment fragment =  null;
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            switch(position) {
                case 0:
                    WebViewFragment webViewFragment = new WebViewFragment();
                    //webViewFragment.getWebView().loadUrl("https://www.sabresonicweb.com/SSW2010/EYM0/checkin.html");
                    return webViewFragment;

                case 1:
                    FlightsFragment flightsFragment = new FlightsFragment();
                    //Bundle args = new Bundle();
                    //args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
                    //flightsFragment.setArguments(args);
                    return flightsFragment;

                case 2:
                    SettingsFragment settingsFragment = new SettingsFragment();
                    //args = new Bundle();
                    //args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
                    //dummySectionFragment.setArguments(args);
                    return settingsFragment;
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "WEBVIEW";//getString(R.string.title_section1).toUpperCase(l);
                case 2:
                    return "SETTINGS";//getString(R.string.title_section3).toUpperCase(l);
                default:
                    return "FLIGHTS";//getString(R.string.title_section2).toUpperCase(l);
            }
            //return null;
        }
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        Toast.makeText(this, "Hi, " + inputText, Toast.LENGTH_SHORT).show();

        //save the username so it can be sent to the server
        username = inputText;

        GCMRegistrar.register(context, SENDER_ID);
        regid = GCMRegistrar.getRegistrationId(context);
        savePrefs();
    }

    public void flightStatusNotification(View v) {

        FlightsDataSource ds = new FlightsDataSource(this);

        ds.open();
        Flight firstFlight = ds.getAllFlights().get(0);
        ds.close();

        String subtitle = "Next Flight Scheduled for " + firstFlight.getOriginScheduled();
        Toast.makeText(this, "Launching Flight Status Notification", Toast.LENGTH_SHORT).show();

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;
        Intent messageDataIntent;


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_plane_gold)
                        .setContentTitle("Flight Status Notification")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(subtitle))
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL);

        /*if(data != null) {
            // Include the JSON message in the notification
            messageDataIntent = new Intent(FlightsFragment.JSON_PAYLOAD_RECEIVED, null, ctx, PushNotificationsFragmentActivity.class);
            messageDataIntent.putExtra("JSONString", data);
            contentIntent = PendingIntent.getActivity(ctx, 0, messageDataIntent , 0);


        } else {
            messageDataIntent = new Intent(FlightsFragment.RECEIVED_MESSAGE, null, ctx, PushNotificationsFragmentActivity.class);
            contentIntent = PendingIntent.getActivity(ctx, 0, messageDataIntent, 0);
        }  */

        //mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(90909, mBuilder.build());


    }

}
