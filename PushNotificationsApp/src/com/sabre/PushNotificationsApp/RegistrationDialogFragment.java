package com.sabre.PushNotificationsApp;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/16/13
 * Time: 1:19 PM
 */
public class RegistrationDialogFragment extends DialogFragment implements TextView.OnEditorActionListener, View.OnClickListener{

    private EditText mEditText;
    private Button mButton;

    public RegistrationDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public void onClick(View v) {
        RegistrationDialogListener activity = (RegistrationDialogListener) getActivity();
        activity.onFinishEditDialog(mEditText.getText().toString());
        this.dismiss();
    }

    public interface RegistrationDialogListener {
        void onFinishEditDialog(String inputText);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container);
        mEditText = (EditText) view.findViewById(R.id.reg_username);
        mEditText.setOnEditorActionListener(this);
        mButton = (Button) view.findViewById(R.id.btn_register);
        mButton.setOnClickListener(this);
        getDialog().setTitle(R.string.menu_registration);
        getDialog().setCancelable(false);

        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text to activity
            RegistrationDialogListener activity = (RegistrationDialogListener) getActivity();
            activity.onFinishEditDialog(mEditText.getText().toString());
            this.dismiss();
            return true;
        }
        return false;
    }
}
