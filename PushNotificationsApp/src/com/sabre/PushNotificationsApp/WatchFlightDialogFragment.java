package com.sabre.PushNotificationsApp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/17/13
 * Time: 1:23 PM
 */
public class WatchFlightDialogFragment extends DialogFragment implements View.OnClickListener {
    private static final String TAG ="WatchFlightDialogFragment";
    private static final String FLIGHT_REGISTRATION_URL = "https://wl16-int.sabresonicweb.com/PushNotificationsWeb_war_exploded/flight_picker";
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = inflater.getContext();

        View layout = View.inflate(context, R.layout.watch_a_flight_dialog, null);
        getDialog().setTitle(R.string.watch_flight);

        //sets values for the gravity spinner
        //easy because they're strings as a resource
        Spinner AirlineSpinner = (Spinner) layout.findViewById(R.id.dialog_watch_airline_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                context, R.array.airline_values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AirlineSpinner.setAdapter(adapter);

        Button btn = (Button) layout.findViewById(R.id.btn_watch);
        btn.setOnClickListener(this);

        return layout;
    }

    public void onClick(View v) {
        String watchedFlightMessage = "Attempting to watch flight " ;
        // after this button is clicked, register in the background
        Dialog d = getDialog();

        d.dismiss();

        Spinner SpinnerAirline = (Spinner) d.findViewById(R.id.dialog_watch_airline_spinner);
        EditText ETflightNumber = (EditText) d.findViewById(R.id.dialog_watch_flight_number);
        DatePicker DPdate = (DatePicker) d.findViewById(R.id.dialog_watch_date_picker);

        String airline = SpinnerAirline.getSelectedItem().toString();
        String number = ETflightNumber.getText().toString();

        // Year is fine, month is zero indexed... so add 1
        // day and month are only one digit if less than 10.
        // Let's fix that.

        String date = DPdate.getYear() + "-";
        int month = (DPdate.getMonth() + 1);
        int day = DPdate.getDayOfMonth();

        if(month < 10) { date += "0"; }
        date += month + "-";
        if(day < 10) { date += "0"; }
        date += day;
        watchedFlightMessage += number + " on airline " + airline + " on date " + date;

        Toast.makeText(d.getContext(), watchedFlightMessage, Toast.LENGTH_SHORT).show();

        registerFlight(airline, number, date);
    }

    @SuppressWarnings("unchecked")
    public void registerFlight(final String airline, final String number, final String date) {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {

                    // You should send the fragment_registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.

                    HttpsURLConnection connection;
                    OutputStreamWriter request = null;

                    //String flight =((EditText) findViewById(R.id.et_flight_picker)).getText().toString();
                    //((EditText) findViewById(R.id.et_flight_picker)).setText("");

                    URL url;
                    String response = null;
                    String parameters = "number="+ number +"&reg_id="+ getUserRegId() + "&date=" + date + "&airline=" + airline;

                    Log.i(TAG, "parameters: " + parameters);

                    url = new URL(FLIGHT_REGISTRATION_URL);
                    connection = (HttpsURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestMethod("POST");

                    request = new OutputStreamWriter(connection.getOutputStream());
                    request.write(parameters);
                    request.flush();
                    request.close();
                    String line = "";
                    InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(isr);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    // Response from server after login process will be stored in response variable.
                    response = sb.toString();
                    //Log.i(TAG, response);
                    // You can perform UI operations here
                    isr.close();
                    reader.close();

                } catch (IOException ex) {
                    msg = "Error: " + ex.toString();
                    Log.e(TAG, "Error registering", ex);
                }
                return msg;
            }

            /*@Override
            protected void onPostExecute(Object o) {
                mDisplay.append(o.toString() + "\n");
            }   */

        }.execute(null, null, null);
    }

    private String getUserRegId(){
        String regid;
        SharedPreferences prefs = context.getSharedPreferences(FlightsFragment.class.getSimpleName(),Context.MODE_PRIVATE);
        regid = prefs.getString(GCMIntentService.PROPERTY_REG_ID, null);

        return regid;
    }

}
