package com.sabre.PushNotificationsApp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/16/13
 * Time: 2:35 PM
 */
public class WeatherDataSource {
    private static final String TAG = "FlightsDataSource";

    // Database fields
    private SQLiteDatabase database;
    private WeatherSQLiteHelper dbHelper;
    private String[] allColumns = { WeatherSQLiteHelper.COLUMN_CODE,
                                    WeatherSQLiteHelper.COLUMN_CITY, WeatherSQLiteHelper.COLUMN_MIN,
                                    WeatherSQLiteHelper.COLUMN_MAX, WeatherSQLiteHelper.COLUMN_CONDITION};

    public WeatherDataSource(Context context) {
        dbHelper = new WeatherSQLiteHelper(context);

        /*Weather w = new Weather();
        w.setCondition("Rainy");
        w.setCity("Conroe");
        w.setCode("JFK");
        w.setMinTemp("66");
        w.setMaxTemp("88");

        open();
        createWeather(w);
        close();

        w.setCondition("Sunny");
        w.setCity("Abu Dhabi");
        w.setCode("AUH");
        w.setMinTemp("79");
        w.setMaxTemp("101");

        open();
        createWeather(w);
        close();

        w.setCondition("Sunny");
        w.setCity("France");
        w.setCode("FRA");
        w.setMinTemp("60");
        w.setMaxTemp("88");

        open();
        createWeather(w);
        close();    */

    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Weather createWeather(Weather weather) {
        Log.i(TAG, "Creating weather id=" + weather.getCode());
        ContentValues values = new ContentValues();
        values.put(WeatherSQLiteHelper.COLUMN_CODE, weather.getCode());
        values.put(WeatherSQLiteHelper.COLUMN_CITY, weather.getCity());
        values.put(WeatherSQLiteHelper.COLUMN_MIN, weather.getMinTemp());
        values.put(WeatherSQLiteHelper.COLUMN_MAX, weather.getMaxTemp());
        values.put(WeatherSQLiteHelper.COLUMN_CONDITION, weather.getCondition());

        //long insertId =
        database.insert(WeatherSQLiteHelper.TABLE_WEATHER, null, values);
        Cursor cursor = database.query(WeatherSQLiteHelper.TABLE_WEATHER,
                allColumns, WeatherSQLiteHelper.COLUMN_CODE + " = \'" + weather.getCode() + "\'", null,
                null, null, null);
        cursor.moveToFirst();
        Weather newWeather = cursorToWeather(cursor);
        cursor.close();
        return newWeather;
    }

    // I don't think this will EVER be used :(
    // never hurts though.
    public void deleteWeather(Weather weather) {
        String code = weather.getCode();
        Log.i(TAG, "Weather deleted for: " + code);
        database.delete(WeatherSQLiteHelper.TABLE_WEATHER, WeatherSQLiteHelper.COLUMN_CODE + " = " + code, null);
    }

    public List<Weather> getAllWeather() {
        List<Weather> weathers = new ArrayList<Weather>();

        Cursor cursor = database.query(WeatherSQLiteHelper.TABLE_WEATHER,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Weather weather = cursorToWeather(cursor);
            weathers.add(weather);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return weathers;
    }

    private Weather cursorToWeather(Cursor cursor) {
        Weather weather = new Weather();

        weather.setCode(cursor.getString(0));
        weather.setCity(cursor.getString(1));
        weather.setMinTemp(cursor.getString(2));
        weather.setMaxTemp(cursor.getString(3));
        weather.setCondition(cursor.getString(4));

        return weather;
    }

    public int updateWeather(Weather weather) {
        ContentValues values = new ContentValues();
        values.put(WeatherSQLiteHelper.COLUMN_CODE, weather.getCode());
        values.put(WeatherSQLiteHelper.COLUMN_CITY, weather.getCity());
        values.put(WeatherSQLiteHelper.COLUMN_MIN, weather.getMinTemp());
        values.put(WeatherSQLiteHelper.COLUMN_MAX, weather.getMaxTemp());
        values.put(WeatherSQLiteHelper.COLUMN_CONDITION, weather.getCondition());

        String whereClause = "code=?";
        String[] whereArgs = {String.valueOf(weather.getCode())};

        int numRows = database.update(WeatherSQLiteHelper.TABLE_WEATHER, values, whereClause, whereArgs);
        Log.i(TAG, "Updated " + numRows + " rows in weather database");

        return numRows;
    }
}
