package com.sabre.PushNotificationsApp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/16/13
 * Time: 2:29 PM
 */
public class WeatherSQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_WEATHER = "weather";

    //public static final String COLUMN_ID = "id";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_CITY = "city_name";
    public static final String COLUMN_MIN = "min_temp";
    public static final String COLUMN_MAX = "max_temp";
    public static final String COLUMN_CONDITION = "condition";

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_WEATHER + "(" + COLUMN_CODE
            + " text primary key, " + COLUMN_CITY
            + " text not null, " + COLUMN_MIN + " text not null, " + COLUMN_MAX
            + " text not null, " + COLUMN_CONDITION + ");";

    public WeatherSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(FlightsSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);
        onCreate(db);
    }

}
