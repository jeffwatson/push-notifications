package com.sabre.PushNotificationsApp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Package: com.sabre.PushNotificationsApp
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/8/13
 * Time: 2:22 PM
 */


/**
 * A fragment that displays a WebView.
 * <p>
 * The WebView is automatically paused or resumed when the Fragment is paused or resumed.
 */
public class WebViewFragment extends Fragment{
    private static final String TAG = "WebViewFragment";
    private static final String BASE_URL = "https://m.sabresonicweb.com/SSW2010/PGM0/webqtrip.html";
    private WebView mWebView;
    private boolean mIsWebViewAvailable;

    public WebViewFragment() {
    }

    /**
     * Called to instantiate the view. Creates and returns the WebView.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mWebView != null) {
            mWebView.destroy();
        }
        mWebView = new WebView(getActivity());
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                Log.i(TAG, "Page finished called!");

                mWebView.loadUrl("javascript:(function() { " +
                        "document.getElementsByClassName('ssci_header')[0].style.display = 'none';" +
                        "document.getElementsByClassName('footer-home')[0].style.display = 'none';" +
                        "})()");

                Log.i(TAG, "removed header and footer!");
            }
        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(BASE_URL);

        mIsWebViewAvailable = true;
        return mWebView;
    }

    /**
     * Called when the fragment is no longer resumed. Pauses the WebView.
     */
    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @Override
    public void onResume() {
        mWebView.onResume();
        super.onResume();
    }

    /**
     * Called when the WebView has been detached from the fragment.
     * The WebView is no longer available after this time.
     */
    @Override
    public void onDestroyView() {
        mIsWebViewAvailable = false;
        super.onDestroyView();
    }

    /**
     * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
     */
    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    /**
     * Gets the WebView.
     */
    public WebView getWebView() {
        return mIsWebViewAvailable ? mWebView : null;
    }


}
