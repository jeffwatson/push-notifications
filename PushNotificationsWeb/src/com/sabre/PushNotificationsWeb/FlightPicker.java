package com.sabre.PushNotificationsWeb;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/26/13
 * Time: 2:04 PM
 */
public class FlightPicker extends HttpServlet {
    private static final String FLIGHT_PICKER_ACTION = "flight";
    private static final String FLIGHT_PICKER_TITLE_SUCCESS = "Registered for flight ";
    private static final String FLIGHT_PICKER_TITLE_FAILURE = "Unable to register for flight ";
    private static final String FLIGHT_PICKER_SUBTITLE = "Touch for details";

    private static final Logger log = Logger.getRootLogger();


    // TODO need to send origin and destination weather data with registration!
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean success = false;

        String reg_id = request.getParameter("reg_id");
        String number = request.getParameter("number");
        String airline = request.getParameter("airline");
        String date = request.getParameter("date");

        log.info("FlightPicker Received request: " + airline+number + " on " + date + " from " + reg_id);

        HashMap<String, String> values = SOAPProcessor.doFlightSOAPRequest(airline, number, date);

        String airline_code = values.get("airline_code");
        String flight_date = values.get("flight_date");
        String flight_number = values.get("flight_number");
        String o_context = values.get("o_context");
        String o_code = values.get("o_code");
        String o_gate = values.get("o_gate");
        String o_scheduled = values.get("o_scheduled");
        String o_scheduled_ind = values.get("o_scheduled_ind");
        String o_terminal = values.get("o_terminal");
        String d_context = values.get("d_context");
        String d_code = values.get("d_code");
        String d_scheduled = values.get("d_scheduled");
        String d_scheduled_ind = values.get("d_scheduled_ind");
        String day_ind = values.get("day_ind");

        String airlineFlightDate = airline_code + flight_number + " " + flight_date;

        log.info("FlightPicker: flight " + number + " is scheduled for: " + o_scheduled);

        if(o_scheduled != null) {
            log.info("updating flight for: " + reg_id);
            Connection c;
            Statement stmt;
            PreparedStatement Pstmt;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
                c.setAutoCommit(false);
                System.out.println("Opened database successfully");

                stmt = c.createStatement();
                String sql = "UPDATE users SET next_flight=next_flight || \' " + airlineFlightDate + "\' WHERE gcm_id=\'" + reg_id +"\';";
                stmt.executeUpdate(sql);
                stmt.close();
                c.commit();
                c.close();

                long newId = PendingMessageProcessor.dbContainsFlight(number, airline, date);

                if(newId == -1l) {
                    c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
                    c.setAutoCommit(false);
                    System.out.println("Opened database successfully again");


                    sql = "INSERT INTO flights VALUES(NULL, \'" + flight_date + "\',\'"
                            + airline_code + "\',\'" + flight_number + "\',\'" + o_context + "\', \'"
                            + o_code +"\', \'" + o_gate + "\', \'" + o_scheduled + "\', \'" + o_scheduled_ind + "\', \'"
                            + o_terminal + "\', \'" + d_context + "\' , \'" + d_code +"\' , \'" + d_scheduled + "\', \'"
                            + d_scheduled_ind + "\' , \'" + day_ind + "\');";
                    log.info("INSERT STATEMENT: " + sql);

                    Pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    Pstmt.executeUpdate();
                    ResultSet rs = Pstmt.getGeneratedKeys();
                    if (rs.next()) {
                        newId = rs.getLong(1);
                        log.info("Got new id of "+ newId);
                        values.put("id", String.valueOf(newId));
                    }

                    stmt.close();
                    c.commit();
                    c.close();
                } else {
                    log.info("Flight was already in the db. id=" + newId);
                    values.put("id", String.valueOf(newId));
                }

                success = true;
            } catch ( Exception e ) {
                log.error("error updating flight for user.", e);
            }
        }


        // TODO fix this, returns all nulls if the weather db doesn't have an entry for that code
        // get origin weather from table
        HashMap<String, String> weatherValues = new HashMap<String, String>();
        try {
            weatherValues = WeatherProcessor.getWeatherValues(values.get("o_code"));
        } catch (Exception e) {
            log.error("unable to get weather values for origin :(", e);
        }

        // put origin weather values in values
        values.put("o_city", weatherValues.get("city_name"));
        values.put("o_min", weatherValues.get("min_temp"));
        values.put("o_max", weatherValues.get("max_temp"));
        values.put("o_condition", weatherValues.get("condition"));

        //get destination weather from table
        try {
            weatherValues = WeatherProcessor.getWeatherValues(values.get("d_code"));
        } catch (Exception e) {
            log.error("unable to get weather values for destination :(", e);
        }

        // put destination weather values in values
        values.put("d_city", weatherValues.get("city_name"));
        values.put("d_min", weatherValues.get("min_temp"));
        values.put("d_max", weatherValues.get("max_temp"));
        values.put("d_condition", weatherValues.get("condition"));

        ArrayList<String> user = new ArrayList<String>();

        user.add(reg_id);

        if(success) {
            PushServer.sendMessage(user, FLIGHT_PICKER_ACTION, FLIGHT_PICKER_TITLE_SUCCESS + airline_code+flight_number,
                    FLIGHT_PICKER_SUBTITLE, PendingMessageProcessor.generateJSONPayload(values));
        } else {
            PushServer.sendMessage(user, FLIGHT_PICKER_ACTION, FLIGHT_PICKER_TITLE_FAILURE + airline_code+flight_number,
                    FLIGHT_PICKER_SUBTITLE, null);
        }

    }

    // REST, Returns the flight data
    // and the weather associated with it.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flight = request.getParameter("flight");
        String airline = request.getParameter("airline");
        String date = request.getParameter("date");

        String JSONData;
        HashMap<String, String> values;

        // Get values from db, flight AND weather
        values = getFlightValues(flight, airline, date);

        // Now get weather
        // get origin weather
        HashMap<String, String> weatherValues;
        weatherValues = getWeatherValues(values.get("o_code"));
        // put these in the map as Origin
        values.put("o_city_name", weatherValues.get("city_name"));
        values.put("o_min_temp", weatherValues.get("min_temp"));
        values.put("o_max_temp", weatherValues.get("max_temp"));
        values.put("o_condition", weatherValues.get("condition"));

        //get Destination weather
        weatherValues = getWeatherValues(values.get("d_code"));
        // put these in the map as Destination
        values.put("d_city_name", weatherValues.get("city_name"));
        values.put("d_min_temp", weatherValues.get("min_temp"));
        values.put("d_max_temp", weatherValues.get("max_temp"));
        values.put("d_condition", weatherValues.get("condition"));

        // Convert to JSON and send the response
        response.setStatus(HttpServletResponse.SC_ACCEPTED);

        JSONObject json = new JSONObject(values);
        JSONData = json.toString();
        System.out.println(JSONData);
        log.info("Sending JSONData: "+ JSONData);
        response.getWriter().write(JSONData);
    }


    // TODO at some point in the future, Hibernate would REALLY help here...
    private HashMap<String, String> getFlightValues(String flightNumber, String airline, String flightDate) {
        System.out.println("getting flightValues from db for: airlinenumber=" + airline + flightNumber + " on: "+ flightDate);
        HashMap<String, String> values = new HashMap<String, String>();

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "SELECT * FROM flights WHERE flight_number=\'" + flightNumber + "\' " +
                    "AND airline_code=\'" + airline + "\' AND flight_date=\'" + flightDate +"\';";
            ResultSet rs = stmt.executeQuery(sql);

            values.put("id", rs.getString("id"));
            values.put("airline_code", rs.getString("airline_code"));
            values.put("flight_date", rs.getString("flight_date"));
            values.put("flight_number", rs.getString("flight_number"));
            values.put("o_context", rs.getString("o_context"));
            values.put("o_code", rs.getString("o_code"));
            values.put("o_gate", rs.getString("o_gate"));
            values.put("o_scheduled", rs.getString("o_scheduled"));
            values.put("o_scheduled_ind", rs.getString("o_scheduled_ind"));
            values.put("o_terminal", rs.getString("o_terminal"));
            values.put("d_context", rs.getString("d_context"));
            values.put("d_code", rs.getString("d_code"));
            values.put("d_scheduled", rs.getString("d_scheduled"));
            values.put("d_scheduled_ind", rs.getString("d_scheduled_ind"));
            values.put("day_ind", rs.getString("day_ind"));
        } catch (Exception e) {
            log.error("Unable to get flight data from REST URL.", e);
        } finally {
            try {
                if(stmt != null) {
                    stmt.close();
                }
                if(c != null) {
                    c.commit();
                    c.close();
                }
            } catch (Exception e) {
                log.error("Error closing weather db.", e);
            }
        }

        log.info("Returning from REST Method values: " + values.toString());
        return values;
    }

    private HashMap<String, String> getWeatherValues(String code) {
        HashMap<String, String> values = new HashMap<String, String>();
        log.info("Getting weather for: "+ code);

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "SELECT * FROM weather WHERE code=\'" + code + "\';";
            ResultSet rs = stmt.executeQuery(sql);

            values.put("code", rs.getString("code"));
            values.put("city_name", rs.getString("city_name"));
            values.put("min_temp", rs.getString("min_temp"));
            values.put("max_temp", rs.getString("max_temp"));
            values.put("condition", rs.getString("condition"));
        } catch (Exception e) {
            log.error("Unable to get weather data from REST URL.", e);
        } finally {
            try {
                if(stmt != null) {
                    stmt.close();
                }
                if(c != null) {
                    c.commit();
                    c.close();
                }
            } catch (Exception e) {
                log.error("Error closing weather db.", e);
            }
        }

        log.info("Returning from REST Weather method values: " + values.toString());

        return values;
    }
}
