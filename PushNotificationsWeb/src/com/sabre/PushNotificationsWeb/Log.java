package com.sabre.PushNotificationsWeb;

//import org.apache.camel.CamelContext;
//import org.apache.camel.impl.DefaultCamelContext;
//import org.apache.camel.util.jndi.JndiContext;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/25/13
 * Time: 9:02 AM
 *
 */

public class Log extends HttpServlet {
    public static String ROOT_OF_PROJECT;
    private static final String pageTitle = "Notification Console!";
    private static Logger log;

    public static String getTitle() {
        return pageTitle;
    }

    public void contextInitialized() {
        ServletContext context = this.getServletContext();
        System.setProperty("rootPath", context.getRealPath("/"));

        log = Logger.getRootLogger();

        ROOT_OF_PROJECT = context.getRealPath("/");
        System.out.println(ROOT_OF_PROJECT);
    }

    public void init() {
        contextInitialized();

        String prefix =  getServletContext().getRealPath("/");
        String file = getInitParameter("log4j-init-file");
        // if the log4j-init-file is not set, then no point in trying
        if(file != null) {
            PropertyConfigurator.configure(prefix + file);
            log.info("Log is initialized!!");
        }
        log.info("This is a test.");
        log.info("Set proxy.");
        System.setProperty("http.proxyHost", "151.193.220.27");
        System.setProperty("http.proxyPort", "80");
    }
}
