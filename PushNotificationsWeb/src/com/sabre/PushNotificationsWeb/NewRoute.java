package com.sabre.PushNotificationsWeb;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.routepolicy.quartz.SimpleScheduledRoutePolicy;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/19/13
 * Time: 4:32 PM
 */

public class NewRoute extends HttpServlet{
    private static final Logger log = Logger.getRootLogger();
    private static final String LOCATION_ACTION = "location";
//    private static final String LOCATION_TITLE = "Location Action!";
//    private static final String LOCATION_SUBTITLE = "Registered for flight with %s at "; // %s=origin || destination;  append the location to the end.

    private static final String WEATHER_ACTION = "weather";
//    private static final String WEATHER_TITLE = "Weather Alert";
//    private static String WEATHER_SUBTITLE = "Temperature is %s in ";

    private static final String TIME_ACTION = "time";
//    private static final String TIME_TITLE = "Time Alert";
//    private static  String TIME_SUBTITLE = "Messaged because ";

    private ApplicationContext context;
    private CamelContext camelContext;


    // TODO use notification_subtitle!
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // TODO find out why this leaks after stopping the project.
        // perhaps there's another way to do this?

        if(camelContext == null) {
            context = new ClassPathXmlApplicationContext("../../WEB-INF/applicationContext.xml");
            camelContext = (CamelContext) context.getBean("camel5");

            try {
                log.info("Stopping and restarting camel.");

                camelContext.stop();
                camelContext.start();
            } catch(Exception e) {
                log.error("Unable to stop and restart camel. ", e);
            }
        }

        Enumeration parameterNames = request.getParameterNames();
        String responseString1 = "";
        String responseString2 = "";
        ArrayList<String> conditions = new ArrayList<String>();
        ArrayList<String> parameters = new ArrayList<String>();

        String start = request.getParameter("start_date");
        String stop = request.getParameter("stop_date");

        Route r = new Route(responseString2);
        r.setStartDate(start);
        r.setStopDate(stop);

        r.writeRouteToDB();

        long routeId = r.getId();

        System.out.println("Created new route with id " + routeId);

        SimpleDateFormat compareFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        long startTime = System.currentTimeMillis();
        long stopTime = System.currentTimeMillis();

        try {
            startTime = compareFormat.parse(start).getTime();
            stopTime = compareFormat.parse(stop).getTime();
        } catch (Exception e) {
            log.error("Unable to parse date while creating route.", e);
        }

        while(parameterNames.hasMoreElements()) {
            String current = (String) parameterNames.nextElement();
            parameters.add(current);
        }

        log.info(parameters.toString());
        System.out.println(parameters.toString());

        for(String parameter: parameters) {
            String[] values = request.getParameterValues(parameter);
            String condition = parameter + "=";
            for(String s: values){
                condition += s + " ";
            }
            conditions.add(condition);
        }

        String[] typeValues = request.getParameterValues("type");
        for(String type: typeValues){
            if(type.equals("location")) {
                String[] airports = request.getParameterValues("airport");
                String location = request.getParameter("location_loc");
                String subtitle = request.getParameter("notification_subtitle");
                String title = request.getParameter("title");
                responseString2 += "location = " + location + " " + Arrays.toString(airports);

                for(String l: airports) {
                    createLocationRoute(l, location, "android", title, subtitle, startTime, stopTime, routeId);
                }

            } else if (type.equals("time")) {

                responseString2 += "time = " + Arrays.toString(request.getParameterValues("time_anchor")) +
                        Arrays.toString(request.getParameterValues("time_condition")) + " " +
                        Arrays.toString(request.getParameterValues("time_value"));

                String anchor = request.getParameter("time_anchor");
                String condition = request.getParameter("time_condition");
                String value = request.getParameter("time_value");
                String notify = "android";
                String title = request.getParameter("title");
                String subtitle = request.getParameter("notification_subtitle");

                createTimeRoute(anchor, condition, value, notify, title, subtitle, startTime, stopTime, routeId);

            } else if (type.equals("weather")) {
                responseString2 += "weather = at "  + Arrays.toString(request.getParameterValues("weather_loc")) + " is "
                        + Arrays.toString(request.getParameterValues("weather_condition"))
                        + " " + Arrays.toString(request.getParameterValues("degrees"));

                String condition = request.getParameter("weather_condition");
                String degrees = request.getParameter("degrees");

                String location = request.getParameter("weather_loc");
                String notify = "android";
                String title = request.getParameter("title");
                String subtitle = request.getParameter("notification_subtitle");

                createWeatherRoute(condition, degrees, location, notify, title, subtitle, startTime, stopTime, routeId);
            } else if (type.equals("before_takeoff")) {
                responseString2 += "time_before_takeoff = "  + Arrays.toString(request.getParameterValues("time_before_takeoff_condition"))
                        + Arrays.toString(request.getParameterValues("before_value"));

                String beforeValue = request.getParameter("before_value");
                String takeoffCondition = request.getParameter("time_before_takeoff_condition");

                String notify = "android";
                String title = request.getParameter("title");
                String subtitle = request.getParameter("notification_subtitle");

                createTimeRoute("before", takeoffCondition, beforeValue, notify, title, subtitle, startTime, stopTime, routeId);
            } else if (type.equals("itinerary") || type.equals("gate")) {
                System.out.println("Creating route for itinerary change.");
                log.info("Creating route for " + type);
                responseString2 += type;

                String notify = "android";
                String title = request.getParameter("title");
                String subtitle = request.getParameter("notification_subtitle");

                createSingleConditionRoute(type, notify, title, subtitle, startTime, stopTime, routeId);
            }
        }

        r.setValue(responseString2);

        r.updateRouteInDb();

        log.info(conditions.toString());
        responseString1 = conditions.toString();

        response.getWriter().write(responseString1 + "<br><br>" + responseString2);
    }

    // TODO do something with the notify preference.
    private void createLocationRoute(final String city, final String location, final String notify, final String title,
                                     final String subtitle, final long startTime, final long stopTime, final long route){
        log.info("Creating new location route for " + location + " city: " + city + " and notifying via " + notify);
        RouteBuilder routeBuilder = null;

        final long timeDelta = startTime - System.currentTimeMillis();

        System.out.println("Delta = " + timeDelta);
        System.out.println("stop time = " + stopTime);

        if(location.equals("o")) {
            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    from("sql:select * from flights where " + //(datetime('now','+2 hours') > o_scheduled) AND" +"
                            "(o_code=\'" + city + "\');" +
                            "?dataSource=push_notifications&batch=true&consumer.delay=15000&consumer.initialDelay=" + timeDelta)
                            .setHeader("location", simple(location))
                            .setHeader("title", simple(title))
                            .setHeader("subtitle", simple(subtitle))
                            .setHeader("route", simple(String.valueOf(route)))
                            .routePolicy(policy)
                            .to("bean:newRoute?method=sayLocationMessage");
                }
            };
        } else if(location.equals("d")) {
            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    from("sql:select * from flights where " + // (datetime('now','+2 hours') > d_scheduled) " +
                            /*"AND */"(d_code=\'" + city + "\');" +
                            "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                            .setHeader("location", simple(location))
                            .setHeader("title", simple(title))
                            .setHeader("subtitle", simple(subtitle))
                            .setHeader("route", simple(String.valueOf(route)))
                            .routePolicy(policy)
                            .to("bean:newRoute?method=sayLocationMessage");
                }
            };
        }

        try {
            camelContext.addRoutes(routeBuilder);
        } catch (Exception e) {
            log.error("Unable to create new location route.", e);
        }
    }

    // TODO do something with the notify preference.
    private void createTimeRoute(final String anchor, final String condition, final String value, final String notify,
                                 final String title, final String subtitle, final long startTime,
                                 final long stopTime, final long route) {
        // takeoff and landing can be done here
        RouteBuilder routeBuilder = null;



        final long timeDelta = startTime - System.currentTimeMillis();

        System.out.println("Delta = " + timeDelta);
        System.out.println("stop time = " + stopTime);

        if(anchor.equals("o_scheduled") || anchor.equals("d_scheduled")) {

            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    if(anchor.equals("o_scheduled")) {
                        from("sql:select * from flights where (datetime(flight_date || ' '  || "+ anchor + ")"
                                + condition  + " datetime('" + value + "'));?dataSource=push_notifications&consumer.delay=15000" +
                                "&batch=true&consumer.initialDelay=" + timeDelta)
                                .setHeader("anchor", simple(anchor))
                                .setHeader("condition", simple(condition))
                                .setHeader("value", simple(value))
                                .setHeader("notify", simple(notify))
                                .setHeader("title", simple(title))
                                .setHeader("subtitle", simple(subtitle))
                                .setHeader("route", simple(String.valueOf(route)))
                                .routePolicy(policy)
                                .to("bean:newRoute?method=sayTimeMessage");

                    } else if (anchor.equals("d_scheduled")){

                        from("sql:select * from flights where (datetime(flight_date||' '||"+ anchor +
                                ", '+'|| ifnull(day_ind, 0) ||' days') " + condition  + " datetime('" + value + "'));" +
                                "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                                .setHeader("anchor", simple(anchor))
                                .setHeader("condition", simple(condition))
                                .setHeader("value", simple(value))
                                .setHeader("notify", simple(notify))
                                .setHeader("title", simple(title))
                                .setHeader("subtitle", simple(subtitle))
                                .setHeader("route", simple(String.valueOf(route)))
                                .routePolicy(policy)
                                .to("bean:newRoute?method=sayTimeMessage");

                    }


                }
            };
        }  else if(anchor.equals("duration")){
            log.info("Creating duration route");
            // this is delay
            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    from("sql:select * from flights;" +
                            "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                            .setHeader("anchor", simple(anchor))
                            .setHeader("condition", simple(condition))
                            .setHeader("value", simple(value))
                            .setHeader("notify", simple(notify))
                            .setHeader("title", simple(title))
                            .setHeader("subtitle", simple(subtitle))
                            .setHeader("route", simple(String.valueOf(route)))
                            .routePolicy(policy)
                            .to("bean:newRoute?method=sayTimeMessage");
                }
            };
        }  else if(anchor.equals("delay")){
            log.info("Creating delay route.");
            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    from("sql:select * from flights;" +
                            "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                            .setHeader("anchor", simple(anchor))
                            .setHeader("condition", simple(condition))
                            .setHeader("value", simple(value))
                            .setHeader("notify", simple(notify))
                            .setHeader("title", simple(title))
                            .setHeader("subtitle", simple(subtitle))
                            .setHeader("route", simple(String.valueOf(route)))
                            .routePolicy(policy)
                            .to("bean:process?method=flightWatcher");
                }
            };

        } else if (anchor.equals("before")) {
            log.info("Creating time before takeoff route.");
            System.out.println("Creating time before takeoff route.");
            routeBuilder = new RouteBuilder() {
                @Override
                public void configure() throws Exception {
                    SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                    policy.setRouteStopDate(new Date(stopTime));
                    policy.setRouteStopRepeatCount(1);
                    policy.setRouteStopRepeatInterval(3000);
                    policy.setRouteStopGracePeriod(10);

                    from("sql:select * from flights WHERE (datetime(flight_date || ' ' || o_scheduled) "+ condition
                            + " datetime('now', '+" + value + " hours'));" +
                            "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                            .setHeader("anchor", simple(anchor))
                            .setHeader("condition", simple(condition))
                            .setHeader("value", simple(value))
                            .setHeader("notify", simple(notify))
                            .setHeader("title", simple(title))
                            .setHeader("subtitle", simple(subtitle))
                            .setHeader("route", simple(String.valueOf(route)))
                            .routePolicy(policy)
                            .to("bean:newRoute?method=sayTimeMessage");
                }
            };


        }

        try {
            camelContext.addRoutes(routeBuilder);
        } catch (Exception e) {
            log.error("Unable to create new time route.", e);
        }

    }

    // TODO do something with the notify preference.
    private void createWeatherRoute(final String condition, final String degrees, final String location,
                                    final String notify, final String title,final String subtitle, final long startTime,
                                    final long stopTime, final long route){

        final long timeDelta = startTime - System.currentTimeMillis();

        System.out.println("Delta = " + timeDelta);
        System.out.println("stop time = " + stopTime);

        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                policy.setRouteStopDate(new Date(stopTime));
                policy.setRouteStopRepeatCount(1);
                policy.setRouteStopRepeatInterval(3000);
                policy.setRouteStopGracePeriod(10);

                from("sql:select * from flights where (datetime('now','+2 hours') > o_scheduled);" +
                        "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                        .setHeader("condition", simple(condition))
                        .setHeader("degrees", simple(degrees))
                        .setHeader("location", simple(location))
                        .setHeader("notify", simple(notify))
                        .setHeader("title", simple(title))
                        .setHeader("subtitle", simple(subtitle))
                        .setHeader("route", simple(String.valueOf(route)))
                        .routePolicy(policy)
                        .to("bean:newRoute?method=sayWeatherMessage");
            }
        };

        try {
            camelContext.addRoutes(routeBuilder);
        } catch (Exception e) {
            log.error("Unable to create new weather route.", e);
        }
    }

    // TODO do something with notify preference
    private void createSingleConditionRoute(final String condition, final String notify,final String title,
                                            final String subtitle, final long startTime, final long stopTime,
                                            final long route) {

        final long timeDelta = startTime - System.currentTimeMillis();

        //System.out.println("Delta = " + timeDelta);
        //System.out.println("stop time = " + stopTime);


        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();

                policy.setRouteStopDate(new Date(stopTime));
                policy.setRouteStopRepeatCount(1);
                policy.setRouteStopRepeatInterval(3000);
                policy.setRouteStopGracePeriod(10);

                from("sql:select * from flights;" +
                        "?dataSource=push_notifications&consumer.delay=15000&batch=true&consumer.initialDelay=" + timeDelta)
                        .setHeader("condition", simple(condition))
                        .setHeader("notify", simple(notify))
                        .setHeader("title", simple(title))
                        .setHeader("subtitle", simple(subtitle))
                        .setHeader("route", simple(String.valueOf(route)))
                        .routePolicy(policy)
                        .to("bean:process?method=singleConditionWatcher");
            }
        };

        try {
            camelContext.addRoutes(routeBuilder);
        } catch (Exception e) {
            log.error("Unable to create new weather route.", e);
        }


    }

    // TODO add units
    public void sayWeatherMessage(Exchange exchange) {

        String result = exchange.getIn().getBody(String.class);
        String condition = (String) exchange.getIn().getHeader("condition");
        String degrees = (String) exchange.getIn().getHeader("degrees");
        String location = (String) exchange.getIn().getHeader("location");
        String notify = (String) exchange.getIn().getHeader("notify");
        String subtitle = (String) exchange.getIn().getHeader("subtitle");
        String title = (String) exchange.getIn().getHeader("title");
        String route = (String) exchange.getIn().getHeader("route");

        log.info("NewRoute SayWeatherMessage received: " + result + condition + degrees + notify + location + subtitle);

        // extract the values for messaging the users.
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_date =  result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));

        flight_number = flight_number.replaceAll("flight_number=", "");
        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_date = flight_date.replaceAll("flight_date=","");

        HashMap<String, String> values = new HashMap<String, String>();
        values.put("flight_number", flight_number);
        values.put("airline_code", airline_code);
        values.put("flight_date", flight_date);
        values.put("action", WEATHER_ACTION);
        values.put("title", title);
        values.put("subtitle", subtitle);
        values.put("route", route);


        String o_code = result.substring(result.indexOf("o_code="), result.indexOf(',', result.indexOf("o_code=")));
        String d_code = result.substring(result.indexOf("d_code="), result.indexOf(',', result.indexOf("d_code=")));

        o_code = o_code.replaceAll("o_code=", "");
        d_code = d_code.replaceAll("d_code=", "");

        String oCurrent = getWeatherValues(o_code).get("current_temp");
        String dCurrent = getWeatherValues(d_code).get("current_temp");

        values.put("o_current", oCurrent);
        values.put("d_current", dCurrent);
        values.put("difference", String.valueOf(Math.abs(Double.valueOf(oCurrent) - Double.valueOf(dCurrent))));

        double value;

        // origin is the lucky one
        if(location.equals("o")) {
            value = Double.valueOf(oCurrent);
            //WEATHER_SUBTITLE += "Origin";
        }
        // use destination
        else if (location.equals("d")) {
            value = Double.valueOf(dCurrent);
            //WEATHER_SUBTITLE += "Destination";
        }
        // do difference
        else {
            value = Math.abs((Double.valueOf(oCurrent) - Double.valueOf(dCurrent)));
            //WEATHER_SUBTITLE = "Temperature difference is %s";
        }

        //compare
        // if the selected temperature is greater than the threshold
        // message the users on the flight.

        if(condition.equals(">")) {
            if(value > Double.valueOf(degrees)) {
                log.info("Weather is greater than threshold! messaging users.");
                //values.put("subtitle", String.format(WEATHER_SUBTITLE, value));
                messageUsersOnFlight(values);
            }
        } else if(condition.equals("<")) {
            if(value < Double.valueOf(degrees)) {
                log.info("Weather is less than threshold! messaging users.");
                //values.put("subtitle", String.format(WEATHER_SUBTITLE, value));
                messageUsersOnFlight(values);
            }
        }
    }

    public void sayLocationMessage(Exchange exchange){
        String result = exchange.getIn().getBody(String.class);

        log.info("SayMessage received: " + result);

        String location = (String) exchange.getIn().getHeader("location");
        String title = (String) exchange.getIn().getHeader("title");
        String subtitle = (String) exchange.getIn().getHeader("subtitle");
        String route = (String) exchange.getIn().getHeader("route");

        // extract the values
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_date =  result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));
        String o_code = result.substring(result.indexOf("o_code="), result.indexOf(',', result.indexOf("o_code=")));
        String d_code = result.substring(result.indexOf("d_code="), result.indexOf(',', result.indexOf("d_code=")));

        flight_number = flight_number.replaceAll("flight_number=", "");
        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_date = flight_date.replaceAll("flight_date=","");
        o_code = o_code.replaceAll("o_code=","");
        d_code = d_code.replaceAll("d_code=","");

        // put the appropriate values in the HashMap
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("flight_number", flight_number);
        values.put("airline_code", airline_code);
        values.put("flight_date", flight_date);
        values.put("action", LOCATION_ACTION);
        values.put("title", title);
        values.put("subtitle", subtitle);
        values.put("route", route);
        values.put("o_code", o_code);
        values.put("d_code", d_code);

        /*if(location.equals("o")) {
            //values.put("subtitle", String.format(LOCATION_SUBTITLE, "origin") + o_code);
        } else if (location.equals("d")) {
            //values.put("subtitle", String.format(LOCATION_SUBTITLE, "destination") + d_code);
        }   */


        // message the users on flight.
        messageUsersOnFlight(values);
    }

    public void sayTimeMessage(Exchange exchange) {
        String result = exchange.getIn().getBody(String.class);
        log.info("sayTimeMessage received: " + result);
        // delay and duration take minutes
        // takeoff and landing take datetime

        String anchor = (String) exchange.getIn().getHeader("anchor");
        String condition = (String) exchange.getIn().getHeader("condition");
        String value = (String) exchange.getIn().getHeader("value");
        String notify = (String) exchange.getIn().getHeader("notify");
        String title = (String) exchange.getIn().getHeader("title");
        String subtitle = (String) exchange.getIn().getHeader("subtitle");
        String route = (String) exchange.getIn().getHeader("route");

        // TODO get all the values here
        String id = result.substring(result.indexOf("id="), result.indexOf(',', result.indexOf("id=")));
        String flight_date = result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));
        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String o_context = result.substring(result.indexOf("o_context="), result.indexOf(',', result.indexOf("o_context=")));
        String o_code = result.substring(result.indexOf("o_code="), result.indexOf(',', result.indexOf("o_code=")));
        String o_gate = result.substring(result.indexOf("o_gate="), result.indexOf(',', result.indexOf("o_gate=")));
        String o_scheduled = result.substring(result.indexOf("o_scheduled="), result.indexOf(',', result.indexOf("o_scheduled=")));
        String o_scheduled_ind = result.substring(result.indexOf("o_scheduled_ind="), result.indexOf(',', result.indexOf("o_scheduled_ind=")));
        String o_terminal = result.substring(result.indexOf("o_terminal="), result.indexOf(',', result.indexOf("o_terminal=")));
        String d_context = result.substring(result.indexOf("d_context="), result.indexOf(',', result.indexOf("d_context=")));
        String d_code = result.substring(result.indexOf("d_code="), result.indexOf(',', result.indexOf("d_code=")));
        String d_scheduled = result.substring(result.indexOf("d_scheduled="), result.indexOf(',', result.indexOf("d_scheduled=")));
        String d_scheduled_ind = result.substring(result.indexOf("d_scheduled_ind="), result.indexOf(',', result.indexOf("d_scheduled_ind=")));
        String day_ind = result.substring(result.indexOf("day_ind="), result.indexOf('}', result.indexOf("day_ind=")));

        id= id.replaceAll("id=", "");
        flight_date = flight_date.replaceAll("flight_date=", "");
        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_number = flight_number.replaceAll("flight_number=", "");
        o_context = o_context.replaceAll("o_context=", "");
        o_code = o_code.replaceAll("o_code=", "");
        o_gate = o_gate.replaceAll("o_gate=", "");
        o_scheduled = o_scheduled.replaceAll("o_scheduled=", "");
        o_scheduled_ind = o_scheduled_ind.replaceAll("o_scheduled_ind=", "");
        o_terminal = o_terminal.replaceAll("o_terminal=", "");
        d_context = d_context.replaceAll("d_context=", "");
        d_code = d_code.replaceAll("d_code=", "");
        d_scheduled = d_scheduled.replaceAll("d_scheduled=", "");
        d_scheduled_ind = d_scheduled_ind.replaceAll("d_scheduled_ind=", "");
        day_ind= day_ind.replaceAll("day_ind=", "");

        try {
            // Day indicator is the last item in the map, treat it special
            day_ind = result.substring(result.indexOf("day_ind="), result.indexOf('}', result.indexOf("day_ind=")));
            day_ind = day_ind.replaceAll("day_ind=", "");
        } catch(Exception e) {
            log.error("Unable to find day Indicator in New Route :(", e);
        }

        HashMap<String, String> values = new HashMap<String, String>();
        values.put("flight_number", flight_number);
        values.put("airline_code", airline_code);
        values.put("flight_date", flight_date);
        values.put("o_code", o_code);
        values.put("o_gate", o_gate);
        values.put("o_scheduled", o_scheduled);
        values.put("o_terminal", o_terminal);
        values.put("d_code", d_code);
        values.put("d_scheduled", d_scheduled);
        values.put("action", TIME_ACTION);
        values.put("title", title);
        values.put("subtitle", subtitle);
        values.put("route", route);
        values.put("delay", "0");
        values.put("takeoff", o_scheduled);
        values.put("landing", d_scheduled);

        SimpleDateFormat compareFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");

        Date oDate;
        Date dDate;

        long oMillis;
        long dMillis;
        long minutes;

        try {
            oDate = compareFormat.parse(flight_date + " " + o_scheduled);
            dDate = compareFormat.parse(flight_date + " " + d_scheduled);


            if (day_ind != null) {
                int days = Integer.valueOf(day_ind);

                Calendar c = Calendar.getInstance();
                c.setTime(oDate);
                c.add(Calendar.DATE, days);  // number of days to add
                oDate = c.getTime();  // dt is now the new date
            }

            log.info("oDate: " + oDate + " dDate: "+ dDate + "value=" + value + " minutes");

            oMillis = oDate.getTime();
            dMillis = dDate.getTime();

            minutes = (Long.valueOf(value) * 60 * 1000);

            values.put("duration", String.valueOf(minutes));

            log.info("oMillis: " + oMillis + " dMillis: "+ dMillis + "minutes=" + minutes);
            if (anchor.equals("duration")) {
                    if(condition.equals("<")) {
                        if(Math.abs(oMillis - dMillis) < minutes) {
                            log.info("< conditions met!");

                            messageUsersOnFlight(values);
                        }
                    } else if (condition.equals(">")) {
                        if(Math.abs(oMillis - dMillis) > minutes) {
                            log.info("> conditions Met!");

                            messageUsersOnFlight(values);
                        }
                    }

            }
        } catch (ParseException e) {
            log.error("Unable to parse date in SayTimeMessage :(", e);
        }



        // Heavy lifting was done by SQL :)
        // send the message straight out!
        if(anchor.equals("o_scheduled") || anchor.equals("d_scheduled") || anchor.equals("before")) {
            messageUsersOnFlight(values);
        }
    }

    // TODO this needs to behave a little differently for flight delays,
    // on update, set sent to 0, and refresh the subtitle and data
    public static boolean messageUsersOnFlight(HashMap<String, String> values) {
        boolean messageSent = false;

        String flight_number = values.get("flight_number");
        String airline_code = values.get("airline_code");
        String flight_date = values.get("flight_date");
        String msg_action = values.get("action");
        String msg_title = values.get("title");
        String msg_data = values.get("data");
        String route = values.get("route");

        String msg_subtitle = generateSubtitle(values);

        // users db contains airline code + number in one field
        String compositeAirlineNumber = airline_code + flight_number + " " + flight_date;

        log.info("finding users on flight " + flight_number);
        log.info("Messaging users on flight " + flight_number);

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM users WHERE next_flight LIKE \'%" + compositeAirlineNumber + "%\';" );
            while ( rs.next() ) {
                String id = rs.getString("id");

                System.out.println("found id of " + id + "!!!");

                Statement findStatement = c.createStatement();
                ResultSet findRS = findStatement.executeQuery("SELECT * from notifications WHERE userID=" + id + " AND routeId=" + route + ";");

                if(!findRS.next()) {
                    // the result was empty, add the notification.
                    System.out.println("Result set was empty!");
                    Statement stmt2 = c.createStatement();
                    String sql = "INSERT INTO notifications VALUES (null, " + id + ", \'" + route +"\', 0, \'" + msg_action +"\' ,"+
                            "\'"+ msg_title +"\', \'" + msg_subtitle + "\', \'" + msg_data +"\');";
                    int result = stmt2.executeUpdate(sql);

                    c.commit();

                    System.out.println("insert result= " + result);

                    stmt2.close();

                    messageSent = true;
                } else {
                    // TODO move this method to flight watcher, if included in the result, set sent to 0 to resend the notification ;)
                    System.out.println("Result Set Was not empty!!!! This notification was already sent! Ignoring this Notification.");
                }

                findRS.close();
                findStatement.close();
            }

            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            log.error("Error in message users on flight.", e);
        }
        return messageSent;
    }

    public static String generateSubtitle(HashMap<String, String> values) {
        System.out.println("Made it to generate subtitle!");
        //String newSubtitle = "";

        String subtitle = values.get("subtitle");
        System.out.println("subtitle= " + subtitle);

        String key;
        String value;

        String pattern = "(\\[\\[.*\\]\\])";

        Pattern mPattern = Pattern.compile(pattern);


        // find all the elements in [[ ]]
        Matcher m = mPattern.matcher(subtitle);
        while(m.find()) {
            System.out.println("pattern was matched!!!");
            System.out.println("group count= " + m.groupCount());
            //System.out.println(m.group(1));

            key = subtitle.substring(subtitle.indexOf("[[")
                    , subtitle.indexOf("]]"));
            key = key.replaceAll("\\[", "");
            System.out.println("key= " + key);

            value = values.get(key);

            System.out.println("value= " + value);

            subtitle = subtitle.replaceAll("(\\[\\[" + key + "\\]\\])", value);

            System.out.println("subtitle= " + subtitle);

            m = mPattern.matcher(subtitle);
            //m.reset();
        }

        return subtitle;
    }

    private HashMap<String, String> getWeatherValues(String code) {
        HashMap<String, String> values = new HashMap<String, String>();
        Connection c;
        Statement stmt;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "SELECT * FROM weather WHERE code=\'" + code + "\';";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                values.put("city_name", rs.getString("city_name"));
                values.put("min_temp", rs.getString("min_temp"));
                values.put("max_temp", rs.getString("max_temp"));
                values.put("current_temp", rs.getString("current_temp"));
                values.put("timestamp", rs.getString("timestamp"));
            }

            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            log.error("Error checking if weather is in db.", e);
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        return values;
    }
}
