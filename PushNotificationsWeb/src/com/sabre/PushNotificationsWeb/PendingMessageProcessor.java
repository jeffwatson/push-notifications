package com.sabre.PushNotificationsWeb;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/24/13
 * Time: 9:34 AM
 */

public class PendingMessageProcessor {
    private static long NOTIFICATION_THRESHOLD = 1000 * 60 * 10; //default ten minute interval
    private static final String FLIGHT_CHANGED_ACTION = "flight";
    private static String FLIGHT_CHANGED_TITLE = "Flight Changed";
    private static String FLIGHT_CHANGED_SUBTITLE = "Click for details";
    private static final Logger log = Logger.getRootLogger();


    public PendingMessageProcessor(){
    }

    public String processMessage(String gcm_id, String action, String title,
                                 String subtitle, String data) {
        String logMsg = "sending xml message to " + gcm_id + " title: "+ title + " sub: " + subtitle + " data: " + data;
        log.info(logMsg);

        ArrayList<String> ids = new ArrayList<String>();
        ids.add(gcm_id);

        PushServer.sendMessage(ids, action, title, subtitle, data);
        return logMsg;
    }


   // TODO this guy needs to take all the headers from the new exchange into account.
    public void flightWatcher(Exchange exchange) {

        String anchor = (String) exchange.getIn().getHeader("anchor"); // I don't think this will be used
        String condition = (String) exchange.getIn().getHeader("condition");  // does it make sense to have a less than value?
        String value = (String) exchange.getIn().getHeader("value");
        String notify = (String) exchange.getIn().getHeader("notify"); // unused currently
        String subtitle = (String) exchange.getIn().getHeader("subtitle");
        String title = (String) exchange.getIn().getHeader("title");
        String route = (String) exchange.getIn().getHeader("route");

        if(!value.equals("")) {
            NOTIFICATION_THRESHOLD = 1000 * 60 * Long.valueOf(value);
        }
        if(!subtitle.equals("")) {
            FLIGHT_CHANGED_SUBTITLE = subtitle;
        }
        if(!title.equals("")) {
            FLIGHT_CHANGED_TITLE = title;
        }

        log.info("Received a list: " + exchange.toString());
        System.out.println("Received a list: " + exchange.toString());

        String result = exchange.getIn().getBody(String.class);


        //TODO might it be beneficial to get all the values?
        // at some point "RULES" are going to be implemented

        String id = result.substring(result.indexOf("id="), result.indexOf(',', result.indexOf("id=")));
        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String flight_date = result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));
        String o_scheduled = result.substring(result.indexOf("o_scheduled="), result.indexOf(',', result.indexOf("o_scheduled=")));

        id= id.replaceAll("id=", "");
        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_number = flight_number.replaceAll("flight_number=", "");
        flight_date = flight_date.replaceAll("flight_date=", "");
        o_scheduled = o_scheduled.replaceAll("o_scheduled=", "");

        String day_ind = null;

        try {
            // Day indicator is the last item in the map, treat it special
            day_ind = result.substring(result.indexOf("day_ind="), result.indexOf('}', result.indexOf("day_ind=")));
            day_ind = day_ind.replaceAll("day_ind=", "");
        } catch(Exception e) {
            log.error("Unable to find day Indicator in New Route :(", e);
        }

        System.out.println("airline= "+ airline_code + ", flightnumber= "+ flight_number + ", flightdate= "+ flight_date + ", o_sched= " + o_scheduled);

        /*String flightNumber = result.substring(result.indexOf("number="), result.lastIndexOf(','));
        flightNumber = flightNumber.replaceAll("number=", "");
        String savedTime = result.substring(result.indexOf("time="), result.lastIndexOf('}'));
        savedTime = savedTime.replaceAll("time=", "");

        System.out.println("flightNumber = "+ flightNumber);
        System.out.println("savedTime= "+ savedTime);*/

        SimpleDateFormat compareFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");

        HashMap<String, String> flightValues = SOAPProcessor.doFlightSOAPRequest(airline_code, flight_number, flight_date);

        // Add the id from the database so id's are the same on the device and server
        flightValues.put("id", id);
        String newOScheduled = flightValues.get("o_scheduled");
        String newDate = flightValues.get("flight_date");
        String origin = flightValues.get("o_code");
        String destination = flightValues.get("d_code");

        flightValues.put("route", route);
        flightValues.put("subtitle", subtitle);
        flightValues.put("title", title);
        flightValues.put("action", "flight");
        flightValues.put("takeoff", flightValues.get("o_scheduled"));
        flightValues.put("landing", flightValues.get("d_scheduled"));

//        flightValues.put;


        Date oDate;
        Date dDate;

        long oMillis;
        long dMillis;
        long minutes;

        try {
            oDate = compareFormat.parse(flight_date + " " + flightValues.get("o_scheduled"));
            dDate = compareFormat.parse(flight_date + " " + flightValues.get("d_scheduled"));


            if (day_ind != null) {
                int days = Integer.valueOf(day_ind);

                Calendar c = Calendar.getInstance();
                c.setTime(oDate);
                c.add(Calendar.DATE, days);  // number of days to add
                oDate = c.getTime();  // dt is now the new date
            }

            log.info("oDate: " + oDate + " dDate: "+ dDate + "value=" + value + " minutes");

            oMillis = oDate.getTime();
            dMillis = dDate.getTime();

            minutes = (Long.valueOf(value) * 60 * 1000);

            flightValues.put("duration", String.valueOf(minutes));

            log.info("oMillis: " + oMillis + " dMillis: "+ dMillis + "minutes=" + minutes);

        } catch (ParseException e) {
            log.error("Unable to parse date in SayTimeMessage :(", e);
        }

        System.out.println("Scheduled time= " + newOScheduled + " Dest: " + destination + " origin: "+ origin);

        long oldTimeMillis = 0l;
        long newTimeMillis = 0l;
        long timeDelta = 0l;

        try {
            oldTimeMillis = compareFormat.parse(flight_date + " " +o_scheduled).getTime();
            System.out.println("oldtime= "+ oldTimeMillis);

            newTimeMillis = compareFormat.parse(newDate + " " + newOScheduled).getTime();
            System.out.println("newTime= "+ newTimeMillis);

            log.info("OldTime= "+ oldTimeMillis);
            log.info("NewTime= "+ newTimeMillis);
        } catch (Exception e) {
            System.out.println("Unable to parse date" + e.getMessage());
            log.error("Unable to parse date.", e);
        }

        timeDelta = Math.abs(newTimeMillis - oldTimeMillis);
        flightValues.put("delay", String.valueOf(timeDelta));

        // TODO take in to account condition
        if(timeDelta >= NOTIFICATION_THRESHOLD) {
            log.info(flight_number +" Difference greater than Threshold!");
            System.out.println(flight_number + " Difference greater than threshold!");

            if(messageUsersOnFlight(flightValues)) {
                log.info("SENT MESSAGE!");
                System.out.println("SENT MESSAGE!");
            }

            updateFlight(flightValues);

        } else if(airline_code != null && flight_number != null && newOScheduled != null && newDate != null){
            System.out.println("Difference less than threshold. Ignoring.");
            log.info("Difference less than threshold. Ignoring.");

            updateFlight(flightValues);
        } else {
            System.out.println("Some values were null. Something went wrong.");
            log.info("Some values were null. Something went wrong.");
        }
    }

    public void singleConditionWatcher(Exchange exchange) {
        String condition = (String) exchange.getIn().getHeader("condition");
        String notify = (String) exchange.getIn().getHeader("notify"); // unused currently
        String subtitle = (String) exchange.getIn().getHeader("subtitle");
        String title = (String) exchange.getIn().getHeader("title");
        String route = (String) exchange.getIn().getHeader("route");

        //System.out.println("Made it to singleConditionWatcher! " + condition);
        log.info("made it to singleConditionWatcher! " + condition);

        /*if(!value.equals("")) {
            NOTIFICATION_THRESHOLD = 1000 * 60 * Long.valueOf(value);
        }
        if(!subtitle.equals("")) {
            FLIGHT_CHANGED_SUBTITLE = subtitle;
        }
        if(!title.equals("")) {
            FLIGHT_CHANGED_TITLE = title;
        }  */

        String result = exchange.getIn().getBody(String.class);

        String id = result.substring(result.indexOf("id="), result.indexOf(',', result.indexOf("id=")));
        String flight_date = result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));
        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String o_context = result.substring(result.indexOf("o_context="), result.indexOf(',', result.indexOf("o_context=")));
        String o_code = result.substring(result.indexOf("o_code="), result.indexOf(',', result.indexOf("o_code=")));
        String o_gate = result.substring(result.indexOf("o_gate="), result.indexOf(',', result.indexOf("o_gate=")));
        String o_scheduled = result.substring(result.indexOf("o_scheduled="), result.indexOf(',', result.indexOf("o_scheduled=")));
        String o_scheduled_ind = result.substring(result.indexOf("o_scheduled_ind="), result.indexOf(',', result.indexOf("o_scheduled_ind=")));
        String o_terminal = result.substring(result.indexOf("o_terminal="), result.indexOf(',', result.indexOf("o_terminal=")));
        String d_context = result.substring(result.indexOf("d_context="), result.indexOf(',', result.indexOf("d_context=")));
        String d_code = result.substring(result.indexOf("d_code="), result.indexOf(',', result.indexOf("d_code=")));
        String d_scheduled = result.substring(result.indexOf("d_scheduled="), result.indexOf(',', result.indexOf("d_scheduled=")));
        String d_scheduled_ind = result.substring(result.indexOf("d_scheduled_ind="), result.indexOf(',', result.indexOf("d_scheduled_ind=")));
        String day_ind = result.substring(result.indexOf("day_ind="), result.indexOf('}', result.indexOf("day_ind=")));


        id= id.replaceAll("id=", "");
        flight_date = flight_date.replaceAll("flight_date=", "");
        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_number = flight_number.replaceAll("flight_number=", "");
        o_context = o_context.replaceAll("o_context=", "");
        o_code = o_code.replaceAll("o_code=", "");
        o_gate = o_gate.replaceAll("o_gate=", "");
        o_scheduled = o_scheduled.replaceAll("o_scheduled=", "");
        o_scheduled_ind = o_scheduled_ind.replaceAll("o_scheduled_ind=", "");
        o_terminal = o_terminal.replaceAll("o_terminal=", "");
        d_context = d_context.replaceAll("d_context=", "");
        d_code = d_code.replaceAll("d_code=", "");
        d_scheduled = d_scheduled.replaceAll("d_scheduled=", "");
        d_scheduled_ind = d_scheduled_ind.replaceAll("d_scheduled_ind=", "");
        day_ind= day_ind.replaceAll("day_ind=", "");

        //System.out.println("airline= "+ airline_code + ", flightnumber= "+ flight_number + ", flightdate= "+ flight_date + ", o_sched= " + o_scheduled);
        log.info("id= " + id + " flight_date= " + flight_date + " flight_number= " + flight_number + " o_context= " +
            o_context + " o_code=" + o_code + " o_gate= " + o_gate + " o_scheduled=" + o_scheduled + " o_scheduled_ind=" + o_scheduled_ind +
            " o_terminal= "+ o_terminal + " d_context= " + d_context + " d_code=" + d_code + " d_scheduled= " + d_scheduled + " d_scheduled_ind=" +
            d_scheduled_ind + " day_ind= "+ day_ind);


        /*String flightNumber = result.substring(result.indexOf("number="), result.lastIndexOf(','));
        flightNumber = flightNumber.replaceAll("number=", "");
        String savedTime = result.substring(result.indexOf("time="), result.lastIndexOf('}'));
        savedTime = savedTime.replaceAll("time=", "");

        System.out.println("flightNumber = "+ flightNumber);
        System.out.println("savedTime= "+ savedTime);*/

        // SimpleDateFormat compareFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");

        HashMap<String, String> flightValues = SOAPProcessor.doFlightSOAPRequest(airline_code, flight_number, flight_date);

        // Add the id from the database so id's are the same on the device and server
        flightValues.put("id", id);
        flightValues.put("route", route);
        flightValues.put("subtitle", subtitle);
        flightValues.put("title", title);
        flightValues.put("action", "flight");

        flightValues.put("g_old", o_gate);
        flightValues.put("g_new", flightValues.get("o_gate"));


        log.info(flightValues.toString());

        // if we're in itinerary change route, compare all the fields,
        // or if we're in gate route, just compare the gates.
        if((condition.equals("itinerary") && (!flight_date.equals(flightValues.get("flight_date"))
                || !airline_code.equals(flightValues.get("airline_code")) || !flight_number.equals(flightValues.get("flight_number"))
                || !o_context.equals(flightValues.get("o_context")) || !o_code.equals(flightValues.get("o_code"))
                || !o_gate.equals(flightValues.get("o_gate")) || !o_scheduled.equals(flightValues.get("o_scheduled"))
                || !o_scheduled_ind.equals(flightValues.get("o_scheduled_ind")) || !o_terminal.equals(flightValues.get("o_terminal"))
                || !d_context.equals(flightValues.get("d_context")) || !d_code.equals(flightValues.get("d_code"))
                || !d_scheduled.equals(flightValues.get("d_scheduled")) || !d_scheduled_ind.equals(flightValues.get("d_scheduled_ind"))
                || !day_ind.equals(flightValues.get("day_ind"))))

                || (condition.equals("gate") && !o_gate.equals(flightValues.get("o_gate")))) {

            log.info(flight_number + " " + condition + " is true!");
            //System.out.println(flight_number + " " + condition + " is true!");

            if(messageUsersOnFlight(flightValues)) {
                log.info("SENT MESSAGE!");
                System.out.println("SENT MESSAGE!");
            }
            // TODO update this method.
            //updateFlight(airline_code, flight_number, newOScheduled, newDate);
            updateFlight(flightValues);

        } else if (condition.equals("cancellation")) {
            // TODO find out how to detect a cancelled flight.
        }
    }

    // TODO this needs to update the whole row!
    private void updateFlight(HashMap<String, String> values) {
        System.out.println("Updating flight " + values.get("airline_code") + values.get("flight_number"));
        log.info("Updating flight " + values.get("airline_code") + values.get("flight_number"));

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            int num = stmt.executeUpdate( "UPDATE flights SET flight_date=\'" + values.get("flight_date")
                    + "\', airline_code=\'" + values.get("airline_code") + "\', flight_number=\'" + values.get("flight_number")
                    + "\', o_context=\'" + values.get("o_context") + "\', o_code=\'" + values.get("o_code")
                    + "\', o_gate=\'" + values.get("o_gate") + "\', o_scheduled=\'" + values.get("o_scheduled")
                    + "\', o_scheduled_ind=\'" + values.get("o_scheduled_ind") + "\', o_terminal=\'" + values.get("o_terminal")
                    + "\', d_context=\'" + values.get("d_context") + "\', d_code=\'" + values.get("d_code")
                    + "\', d_scheduled=\'" + values.get("d_scheduled") + "\', d_scheduled_ind=\'" + values.get("d_scheduled_ind")
                    + "\', day_ind=\'" + values.get("day_ind") + "\' "
                    + "WHERE id=\'" + values.get("id") + "\';" );
            log.info("updated " + num + " rows");
            System.out.println("updated "+ num + " rows");

            c.commit();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            log.error("Error while updating flight_data", e);
        }
    }

    /**
     * This method will check if the flight is in the database, and return it's id if it is included.
     * otherwise it will return -1
     *
     * @param number the flight number
     * @param airline the airine the flight is on
     * @param date the date of the flight
     * @return the id of the flight if it is in the database, -1 otherwise
     */
    public static long dbContainsFlight(String number, String airline, String date) {
        long id = -1l;

        log.info("Checking if db contains " + airline+number);

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "SELECT * FROM flights WHERE flight_number=\'" + number
                    + "\' AND airline_code=\'"+ airline + "\' AND flight_date=\'"+ date + "\';";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                id = rs.getInt("id");
                log.info("Got id of "+ id);
            }

            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            log.error("Error checking if flight is in db.", e);
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        log.info("flightNumber: "+ airline+number + " in db at row: "+ id);
        return id;
    }

    // TODO need to force this method to reset sent to 0 if this happens again.

    public static String generateJSONPayload(HashMap<String, String> values) {
        String JSONPayload = "";

        String id = values.get("id");
        String airline_code = values.get("airline_code");
        String flight_date = values.get("flight_date");
        String flight_number = values.get("flight_number");
        String o_context = values.get("o_context");
        String o_code = values.get("o_code");
        String o_gate = values.get("o_gate");
        String o_scheduled = values.get("o_scheduled");
        String o_scheduled_ind = values.get("o_scheduled_ind");
        String o_terminal = values.get("o_terminal");
        String d_context = values.get("d_context");
        String d_code = values.get("d_code");
        String d_scheduled = values.get("d_scheduled");
        String d_scheduled_ind = values.get("d_scheduled_ind");
        String day_ind = values.get("day_ind");

        String o_city = values.get("o_city");
        String o_min = values.get("o_min");
        String o_max = values.get("o_max");
        String o_condition = values.get("o_condition");

        String d_city = values.get("d_city");
        String d_min = values.get("d_min");
        String d_max = values.get("d_max");
        String d_condition = values.get("d_condition");

        JSONObject mObject = new JSONObject();

        mObject.put("id", id);
        mObject.put("airline_code", airline_code);
        mObject.put("flight_date", flight_date);
        mObject.put("flight_number", flight_number);
        mObject.put("o_context", o_context);
        mObject.put("o_code", o_code);
        mObject.put("o_gate", o_gate);
        mObject.put("o_scheduled", o_scheduled);
        mObject.put("o_scheduled_ind", o_scheduled_ind);
        mObject.put("o_terminal", o_terminal);
        mObject.put("d_context", d_context);
        mObject.put("d_code", d_code);
        mObject.put("d_scheduled", d_scheduled);
        mObject.put("d_scheduled_ind", d_scheduled_ind);
        mObject.put("day_ind", day_ind);

        mObject.put("o_city", o_city);
        mObject.put("o_min", o_min);
        mObject.put("o_max", o_max);
        mObject.put("o_condition", o_condition);

        mObject.put("d_city", d_city);
        mObject.put("d_min", d_min);
        mObject.put("d_max", d_max);
        mObject.put("d_condition", d_condition);

        JSONPayload = mObject.toJSONString();

        return JSONPayload;
    }

    public void notificationWatch(Exchange exchange) {
        System.out.println("Made it to notificationWatch!!");

        System.out.println(exchange.getIn().getBody(String.class));

        String result = exchange.getIn().getBody(String.class);

        String id = result.substring(result.indexOf("id="), result.indexOf(',', result.indexOf("id=")));
        String userID = result.substring(result.indexOf("userID="), result.indexOf(',', result.indexOf("userID=")));
        String routeId= result.substring(result.indexOf("routeId="), result.indexOf(',', result.indexOf("routeId=")));
        String sent = result.substring(result.indexOf("sent="), result.indexOf(',', result.indexOf("sent=")));
        String action = result.substring(result.indexOf("action="), result.indexOf(',', result.indexOf("action=")));
        String title = result.substring(result.indexOf("title="), result.indexOf(',', result.indexOf("title=")));
        String subtitle = result.substring(result.indexOf("subtitle="), result.indexOf(',', result.indexOf("subtitle=")));
        String data = result.substring(result.indexOf("data="), result.indexOf(',', result.indexOf("data=")));
        String gcm_id = result.substring(result.indexOf("gcm_id="), result.indexOf(',', result.indexOf("gcm_id=")));
        String notifID = result.substring(result.indexOf("notifID="), result.indexOf('}', result.indexOf("notifID=")));

        id = id.replaceAll("id=", "");
        userID = userID.replaceAll("userID=", "");
        routeId = routeId.replaceAll("routeId=", "");
        sent = sent.replaceAll("sent=", "");
        action = action.replaceAll("action=", "");
        title = title.replaceAll("title=", "");
        subtitle = subtitle.replaceAll("subtitle=", "");
        data = data.replaceAll("data=", "");
        gcm_id = gcm_id.replaceAll("gcm_id=", "");
        notifID = notifID.replaceAll("notifID=", "");

        System.out.println("notificationWatch: id=" + id + " user=" + userID + " routeID=" + routeId + " sent=" + sent
                + " action=" + action + " title= " + title + " subtitle= " + subtitle + " data= "+ data
                + " gcm_id= " + gcm_id + " notifID= " + notifID);

        Connection c;
        Statement stmt;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            int num = stmt.executeUpdate( "UPDATE notifications SET sent=1 WHERE id=" + notifID +";" );
            log.info("updated " + num + " rows");
            System.out.println("updated "+ num + " rows");

            c.commit();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            log.error("Error while updating flight_data", e);
        }

        processMessage(gcm_id, action, title, subtitle, data);
    }

    public static boolean messageUsersOnFlight(HashMap<String, String> values) {
        boolean messageSent = false;

        String flight_number = values.get("flight_number");
        String airline_code = values.get("airline_code");
        String flight_date = values.get("flight_date");
        String msg_action = values.get("action");
        String msg_title = values.get("title");
        String msg_data = values.get("data");
        String route = values.get("route");

        String msg_subtitle = NewRoute.generateSubtitle(values);

        // users db contains airline code + number in one field
        String compositeAirlineNumber = airline_code + flight_number + " " + flight_date;

        log.info("finding users on flight " + flight_number);
        log.info("Messaging users on flight " + flight_number);

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM users WHERE next_flight LIKE \'%" + compositeAirlineNumber + "%\';" );
            while ( rs.next() ) {
                String id = rs.getString("id");

                System.out.println("found id of " + id + "!!!");

                Statement findStatement = c.createStatement();
                ResultSet findRS = findStatement.executeQuery("SELECT * from notifications WHERE userID=" + id + " AND routeId=" + route + ";");

                if(!findRS.next()) {
                    // the result was empty, add the notification.
                    System.out.println("Result set was empty!");
                    Statement stmt2 = c.createStatement();
                    String sql = "INSERT INTO notifications VALUES (null, " + id + ", \'" + route +"\', 0, \'" + msg_action +"\' ,"+
                            "\'"+ msg_title +"\', \'" + msg_subtitle + "\', \'" + msg_data +"\');";
                    int result = stmt2.executeUpdate(sql);

                    c.commit();

                    System.out.println("insert result= " + result);

                    stmt2.close();

                    messageSent = true;
                } else {
                    String notifID = findRS.getString("id");
                    System.out.println("Result Set Was not empty!!!! Updating notification id=" + notifID);

                    // the result was empty, add the notification.
                    System.out.println("Result set was empty!");
                    Statement stmt2 = c.createStatement();
                    String sql = "UPDATE notifications SET sent=0 WHERE id=" + notifID + ";";
                    int result = stmt2.executeUpdate(sql);

                    c.commit();

                    System.out.println("update result= " + result);

                    stmt2.close();

                    messageSent = true;
                }

                findRS.close();
                findStatement.close();
            }

            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            log.error("Error in message users on flight.", e);
        }
        return messageSent;
    }
}
