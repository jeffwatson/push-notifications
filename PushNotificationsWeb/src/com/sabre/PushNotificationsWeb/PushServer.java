package com.sabre.PushNotificationsWeb;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/21/13
 * Time: 11:48 AM
 */
public class PushServer {
    private static final Logger log = Logger.getRootLogger();
    private static final String GCM_API_KEY = "AIzaSyCG0znP4YAMbruFvZi7LMR6fIyXCa6I1OA";

    public static boolean sendMessage(ArrayList<String> ids, String action, String title, String subtitle, String data) {
        System.setProperty("https.proxyHost", "151.193.220.27");
        System.setProperty("https.proxyPort", "80");
        System.setProperty("https.proxyUser", "sg0218937");
        System.setProperty("https.proxyPassword", "trinity2");

        boolean success = false;
        System.out.println(ids.toString());
        System.out.println("Action: " + action + ", Title: " + title + ", Subtitle: " + subtitle + ", data: " + data);
        log.info("Action: " + action + ", Title: " + title + ", Subtitle: " + subtitle + ", data: " + data);

        try {
            //API KEY
            Sender sender = new Sender(GCM_API_KEY);

            // use this line to send message with payload data
            Message message = new Message.Builder()
                    .collapseKey("1")
                    .timeToLive(20)
                    .delayWhileIdle(true)
                    .addData("action", action)
                    .addData("title", title)
                    .addData("subtitle", subtitle)
                    .addData("data", data)
                    .build();

            // Use this for multicast messages
            MulticastResult result = sender.send(message, ids, 1);
            log.info(result.toString());

            if (result.getResults() != null) {
                int canonicalRegId = result.getCanonicalIds();
                if (canonicalRegId != 0) {
                    log.info("sent to user: "+ canonicalRegId);
                    System.out.println("canonical reg id  =" + canonicalRegId);
                }
                success = true;
            } else {
                int error = result.getFailure();
                log.info("Unable to send. Error: " + error);
            }

        } catch (Exception e) {
            log.info(e.fillInStackTrace().toString());
        }

        return success;
    }
}
