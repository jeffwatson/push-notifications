package com.sabre.PushNotificationsWeb;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/24/13
 * Time: 9:35 AM
 */

public class Registration extends HttpServlet {
    private static final String REGISTRATION_ACTION = "registration";
    private static final String REGISTRATION_TITLE = "Registration Complete";
    private static final String REGISTRATION_SUBTITLE = "Welcome ";


    private static final Logger log = Logger.getRootLogger();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String user = request.getParameter("user");
        String reg_id = request.getParameter("reg_id");

        User newUser = new User(user, reg_id);
        log.info("User Created: " + newUser.getUsername());
        ArrayList<String> thisUser = new ArrayList<String>();
        thisUser.add(newUser.getReg_id());

        PushServer.sendMessage(thisUser, REGISTRATION_ACTION, REGISTRATION_TITLE,
                REGISTRATION_SUBTITLE + newUser.getUsername(), null);
    }
}
