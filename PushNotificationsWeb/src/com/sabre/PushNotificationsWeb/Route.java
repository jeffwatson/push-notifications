package com.sabre.PushNotificationsWeb;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/26/13
 * Time: 3:00 PM
 */
public class Route {
    private static final Logger log = Logger.getRootLogger();

    private long id;
    private String value;
    private String startDate;
    private String stopDate;

    public Route() {
        // empty constructor
    }

    public Route (String value) {
        this.value = value;
    }

    public Route(long id, String value) {
        this.id = id;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStopDate() {
        return stopDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }



    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }

    public static List<Route> getRoutes() {
        List<Route> routes = new ArrayList<Route>();

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM routes;" );
            while ( rs.next() ) {
                Route r = new Route();

                long id = rs.getLong("id");
                String value = rs.getString("value");
                String start = rs.getString("start_date");
                String stop = rs.getString("stop_date");

                r.setId(id);
                r.setValue(value);
                r.setStartDate(start);
                r.setStopDate(stop);

                routes.add(r);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            log.error("Error getting routes from db.", e);
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

       return routes;
    }

    public void writeRouteToDB() {
        log.info("attempting to save route: " + this.toString());

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "INSERT INTO routes VALUES (null, \'" + this.getValue() + "\', \'" + this.getStartDate() + "\', \'" + this.getStopDate() + "\');";
            stmt.executeUpdate(sql);

            this.setId(stmt.getGeneratedKeys().getInt(1));
            System.out.println("Inserted new route at row " + this.getId());

            stmt.close();
            c.commit();
            c.close();
        } catch ( Exception e ) {
            log.error("error adding route.", e);
        }
    }

    public void updateRouteInDb() {
        log.info("attempting to save route: " + this.toString());

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "UPDATE routes SET value = \'" + this.getValue() + "\', start_date=\'" + this.getStartDate() + "\' ," +
                    "stop_date=\'" + this.getStopDate() + "\' WHERE id=" + this.getId() + ";";
            stmt.executeUpdate(sql);

            //this.setId(stmt.getGeneratedKeys().getInt(1));
            //System.out.println("Inserted new route at row " + this.getId());

            stmt.close();
            c.commit();
            c.close();
        } catch ( Exception e ) {
            log.error("error adding route.", e);
        }

    }
}
