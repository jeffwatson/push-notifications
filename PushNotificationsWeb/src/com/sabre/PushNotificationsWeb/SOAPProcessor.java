package com.sabre.PushNotificationsWeb;

import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.w3c.dom.Node;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 7/31/13
 * Time: 5:15 PM
 */
public class SOAPProcessor {
    private static String binarySecurityToken = "";
    private static final Logger log = Logger.getRootLogger();

    public static HashMap<String, String> doFlightSOAPRequest(String airline, String number, String date) {
        log.info("Doing soap request... a: " + airline + ", #: " + number + ", date: " + date);

        HashMap<String, String> values = new HashMap<String, String>();

        System.setProperty("http.proxyHost", "151.193.220.27");
        System.setProperty("http.proxyPort", "80");

        // TODO get rid of this line when I'm done for today!
        //binarySecurityToken = "Shared/IDL:IceSess/SessMgr:1.0.IDL/Common/!ICESMS/ACPCRTD!ICESMSLB/CRT.LB!-3746392106686599931!542795!0";

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            String url = "http://sws-crt.internal.cert.sabre.com";

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(airline, number, date), url);
            soapConnection.close();

            // Process the SOAP Response
            printSOAPResponse(soapResponse);

            values = findFlightValues(soapResponse);
        } catch (Exception e) {
            log.error("Error occurred while sending SOAP Request to server", e);
        }

        return values;
    }

    private static HashMap<String, String> findFlightValues(SOAPMessage soapResponse) {
        HashMap<String, String> soapValues = new HashMap<String, String>();
        String airline_code = null, flight_date = null, flight_number = null,
                o_context = null, o_code = null, o_gate = null, o_scheduled = null, o_scheduled_ind = null,
                o_terminal = null, d_context = null, d_code = null, d_scheduled = null, d_scheduled_ind = null,
                day_ind = null;

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr;
        try{
            Document resp = toDocument(soapResponse);

            expr = xpath.compile("*[local-name() = 'Envelope']/*[local-name() = 'Body']/" +
                    "*[local-name() = 'OTA_AirFlifoRS']/*[local-name() = 'FlightInfoDetails']");

            Node node = (Node) expr.evaluate(resp, XPathConstants.NODE);

            NamedNodeMap nnm = node.getAttributes();

            // this gets the airline code,  date, and flight number (although we *should* already have these...)
            airline_code = nnm.getNamedItem("AirlineCode").getNodeValue();
            flight_date = nnm.getNamedItem("DateTime").getNodeValue();
            flight_number = nnm.getNamedItem("FlightNumber").getNodeValue();

            // Ignoring namespaces, finds path of:
            // /*:Envelope/*:Body/*:OTA_AirFlifoRS/*:FlightInfoDetails/*:ScheduledInfo/*:FlightLegInfo
            expr = xpath.compile("*[local-name() = 'Envelope']/*[local-name() = 'Body']" +
                    "/*[local-name() = 'OTA_AirFlifoRS']/*[local-name() = 'FlightInfoDetails']" +
                    "/*[local-name() = 'ScheduledInfo']/*[local-name() = 'FlightLegInfo']");

            node = (Node) expr.evaluate(resp, XPathConstants.NODE);

            Node innerNode;
            NamedNodeMap innerNNM;

            while (node != null) {
                nnm = node.getAttributes();

                if(o_code == null && o_context == null) {
                    try {
                        o_code = nnm.getNamedItem("LocationCode").getNodeValue();
                        o_context = nnm.getNamedItem("CodeContext").getNodeValue();

                        // get the 'DepartureDateTime' node inside of this one.
                        expr = xpath.compile("*[local-name() = 'DepartureDateTime']");
                        innerNode = (Node) expr.evaluate(node, XPathConstants.NODE);
                        innerNNM = innerNode.getAttributes();

                        o_gate = innerNNM.getNamedItem("Gate").getNodeValue();
                        o_scheduled = innerNNM.getNamedItem("Scheduled").getNodeValue();
                        o_scheduled_ind = innerNNM.getNamedItem("ScheduledInd").getNodeValue();
                        o_terminal = innerNNM.getNamedItem("Terminal").getNodeValue();
                    } catch (Exception e) {
                        log.info("Origin values not found in this node...");
                    }
                } else {
                    try {
                        d_code = nnm.getNamedItem("LocationCode").getNodeValue();
                        d_context = nnm.getNamedItem("CodeContext").getNodeValue();

                        // get the 'ArrivalDateTime' node inside of this one.
                        expr = xpath.compile("*[local-name() = 'ArrivalDateTime']");
                        innerNode = (Node) expr.evaluate(node, XPathConstants.NODE);
                        innerNNM = innerNode.getAttributes();

                        d_scheduled = innerNNM.getNamedItem("Scheduled").getNodeValue();
                        d_scheduled_ind = innerNNM.getNamedItem("ScheduledInd").getNodeValue();

                        // get the 'DayIndicator' node inside of this one.
                        expr = xpath.compile("*[local-name() = 'TPA_Extensions']/*[local-name() = 'DayIndicator']");
                        innerNode = (Node) expr.evaluate(innerNode, XPathConstants.NODE);

                        innerNNM = innerNode.getAttributes();

                        day_ind = innerNNM.getNamedItem("Value").getNodeValue();
                    } catch (Exception e) {
                        log.info("Destination values not found in this node...");
                    }
                }

                node = node.getNextSibling();
            }

            System.out.println("airline: " + airline_code + " flight_date: " + flight_date + " flight_number: "+flight_number +
                " o_cxt: "+ o_context + " o_code: "+ o_code + " o_gate: "+ o_gate + " o_sched: "+ o_scheduled + " o_sched_ind: "+ o_scheduled_ind +
                " o_term: "+ o_terminal + " d_ctx: "+ d_context + " d_code: "+ d_code + " d_sched: "+ d_scheduled + " d_sched_ind: " + d_scheduled_ind +
                " day_ind: " + day_ind);

            log.info("airline: " + airline_code + " flight_date: " + flight_date + " flight_number: "+flight_number +
                    " o_cxt: "+ o_context + " o_code: "+ o_code + " o_gate: "+ o_gate + " o_sched: "+ o_scheduled + " o_sched_ind: "+ o_scheduled_ind +
                    " o_term: "+ o_terminal + " d_ctx: "+ d_context + " d_code: "+ d_code + " d_sched: "+ d_scheduled + " d_sched_ind: " + d_scheduled_ind +
                    " day_ind: " + day_ind);

            soapValues.put("airline_code", airline_code);
            soapValues.put("flight_date", flight_date);
            soapValues.put("flight_number", flight_number);
            soapValues.put("o_context", o_context);
            soapValues.put("o_code", o_code);
            soapValues.put("o_gate", o_gate);
            soapValues.put("o_scheduled", o_scheduled);
            soapValues.put("o_scheduled_ind", o_scheduled_ind);
            soapValues.put("o_terminal", o_terminal);
            soapValues.put("d_context", d_context);
            soapValues.put("d_code", d_code);
            soapValues.put("d_scheduled", d_scheduled);
            soapValues.put("d_scheduled_ind", d_scheduled_ind);
            soapValues.put("day_ind", day_ind);

        } catch (Exception err) {
            log.error("Unable to find info in document.", err);
            err.printStackTrace();
        }

        return soapValues;
    }

    /**
     * Method used to print the SOAP Response
     */
    private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        log.info("\nResponse SOAP Message = ");
        log.info(getStringFromDoc(toDocument(soapResponse)));

        System.out.println("Response SOAP Message = " + getStringFromDoc(toDocument(soapResponse)));
    }

    private static SOAPMessage getSoapMessageFromString(String xml) throws SOAPException, IOException {
        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-16"))));
        return message;
    }

    private static Document toDocument(SOAPMessage soapMsg)
            throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
        Source src = soapMsg.getSOAPPart().getContent();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMResult result = new DOMResult();
        transformer.transform(src, result);
        return (Document)result.getNode();
    }

    public String setBinarySecurityToken(@Header("token") String token) {
        binarySecurityToken = token;

        System.out.println("BINARY SECURITY TOKEN= "+ binarySecurityToken);
        log.info("BINARY SECURITY TOKEN= " + binarySecurityToken);
        return binarySecurityToken;
    }

    public static String getStringFromDoc(org.w3c.dom.Document doc)    {
        DOMImplementationLS domImplementation = (DOMImplementationLS) doc.getImplementation();
        LSSerializer lsSerializer = domImplementation.createLSSerializer();
        return lsSerializer.writeToString(doc);
    }

    public void log(String string) {
        System.out.println("From the bean");
        log.info("from the bean" + string);
    }

    /* TODO find out what this did.
       I don't think it has a purpose any more.  */
    public String sessionCreate(String RQ) {
        String sessionRS = null;

        System.out.println("In session Create, set proxy.");
        log.info("In session create, set proxy.");
        System.setProperty("http.proxyHost", "151.193.220.27");
        System.setProperty("http.proxyPort", "80");

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://sws-crt.internal.cert.sabre.com";

            //SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(airline, number), url);

            SOAPMessage soapResponse = soapConnection.call(getSoapMessageFromString(RQ),url);

            // Process the SOAP Response
            // printSOAPResponse(soapResponse);

            sessionRS = getStringFromDoc(toDocument(soapResponse));

            //values = findFlightValues(soapResponse);

            //XPath xpath = XPathFactory.newInstance().newXPath();
            //xpath.setNamespaceContext(new SoapNamespaceContext());
            //Node resultNode = (Node) xpath.evaluate("//m:GetWhoISResult", soapResponse.getSOAPBody(), XPathConstants.NODE);

            soapConnection.close();
        } catch (Exception e) {
            log.error("Error occurred while sending SOAP Request to server", e);
            //System.err.println("Error occurred while sending SOAP Request to Server");
            //e.printStackTrace();
        }

        return sessionRS;
    }

    // TODO it would be nice if this created an *actual* SOAPMessage
    private static SOAPMessage createSOAPRequest(String airline, String number, String date) {
        System.out.println("Entering createSOAPRequest");
        SOAPMessage request = null;

        String rq = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "\t<soap:Header>\n" +
                "\t\t<ns4:MessageHeader xmlns:ns4=\"http://www.ebxml.org/namespaces/messageHeader\" xmlns:ns5=\"http://www.w3.org/1999/xlink\" xmlns:ns6=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns7=\"http://webservices.sabre.com/sabreXML/2003/07\" xmlns:ns8=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">\n" +
                "\t\t\t<ns4:From>\n" +
                "\t\t\t\t<ns4:PartyId>99999</ns4:PartyId>\n" +
                "\t\t\t</ns4:From>\n" +
                "\t\t\t<ns4:To>\n" +
                "\t\t\t\t<ns4:PartyId>123123</ns4:PartyId>\n" +
                "\t\t\t</ns4:To>\n" +
                "\t\t\t<ns4:CPAId>EY</ns4:CPAId>\n" +
                "\t\t\t<ns4:ConversationId>EYM0_E6E08C4FC02F7CE2DFD1BCBE17AB5DD8</ns4:ConversationId>\n" +
                "\t\t\t<ns4:Service>AirFlifo</ns4:Service>\n" +
                "\t\t\t<ns4:Action>OTA_AirFlifoLLSRQ</ns4:Action>\n" +
                "\t\t\t<ns4:MessageData>\n" +
                "\t\t\t\t<ns4:MessageId>mid:2013-07-30T18:03:09@eb2.com</ns4:MessageId>\n" +
                "\t\t\t\t<ns4:Timestamp>2013-07-30T18:03:09</ns4:Timestamp>\n" +
                "\t\t\t</ns4:MessageData>\n" +
                "\t\t</ns4:MessageHeader>\n" +
                "\t\t<ns8:Security xmlns:ns4=\"http://www.ebxml.org/namespaces/messageHeader\" xmlns:ns5=\"http://www.w3.org/1999/xlink\" xmlns:ns6=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns7=\"http://webservices.sabre.com/sabreXML/2003/07\" xmlns:ns8=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">\n" +
                "\t\t\t<ns8:BinarySecurityToken>"+binarySecurityToken + "</ns8:BinarySecurityToken>\n" +
                "\t\t</ns8:Security>\n" +
                "\t</soap:Header>\n" +
                "\t<soap:Body>\n" +
                "\t\t<ns7:OTA_AirFlifoRQ Version=\"2003A.TsabreXML1.5.1\" xmlns:ns4=\"http://www.ebxml.org/namespaces/messageHeader\" xmlns:ns5=\"http://www.w3.org/1999/xlink\" xmlns:ns6=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns7=\"http://webservices.sabre.com/sabreXML/2003/07\" xmlns:ns8=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">\n" +
                "\t\t\t<ns7:POS>\n" +
                "\t\t\t\t<ns7:Source/>\n" +
                "\t\t\t</ns7:POS>\n" +
                "\t\t\t<ns7:Airline Code=\"" + airline + "\"/>\n" +
                "\t\t\t<ns7:FlightNumber>" + number + "</ns7:FlightNumber>\n" +
                "\t\t\t<ns7:DepartureDate>" + date + "</ns7:DepartureDate>\n" +
                "\t\t</ns7:OTA_AirFlifoRQ>\n" +
                "\t</soap:Body>\n" +
                "</soap:Envelope>";

        try {
            InputStream is = new ByteArrayInputStream(rq.getBytes());
            request = MessageFactory.newInstance().createMessage(null, is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }
    // FIXME this is a more "proper" way to create the soap message...
    // but it is broken.
    // using the string way for now.

    /*private static SOAPMessage createSOAPRequest(String airline, String flightNumber) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        //soapMessage.
        soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        //String serverURI = "http://ws.cdyne.com/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        //SOAP Header
        SOAPHeader header = soapMessage.getSOAPHeader();
        header.addNamespaceDeclaration("ns5", "http://www.ebxml.org/namespaces/messageHeader");
        header.addNamespaceDeclaration("ns8", "http://schemas.xmlsoap.org/ws/2002/12/secext");

        SOAPElement soapMessageHeaderElem = (SOAPElement) header.addChildElement("MessageHeader", "ns5");
        soapMessageHeaderElem.addNamespaceDeclaration("ns4", "http://webservices.sabre.com/sabreXML/2003/07");
        soapMessageHeaderElem.addNamespaceDeclaration("ns5", "http://www.ebxml.org/namespaces/messageHeader");
        soapMessageHeaderElem.addNamespaceDeclaration("ns6", "http://www.w3.org/2000/09/xmldsig#");
        soapMessageHeaderElem.addNamespaceDeclaration("ns7", "http://www.w3.org/1999/xlink");
        soapMessageHeaderElem.addNamespaceDeclaration("ns8", "http://schemas.xmlsoap.org/ws/2002/12/secext");

        SOAPElement soapMessageHeaderFrom = soapMessageHeaderElem.addChildElement("From", "ns5");
        SOAPElement soapMessageHeaderPartyIdFrom = soapMessageHeaderFrom.addChildElement("PartyId", "ns5");
        soapMessageHeaderPartyIdFrom.addTextNode("99999");

        SOAPElement soapMessageHeaderTo = soapMessageHeaderElem.addChildElement("To", "ns5");
        SOAPElement soapMessageHeaderPartyIdTo = soapMessageHeaderTo.addChildElement("PartyId", "ns5");
        soapMessageHeaderPartyIdTo.addTextNode("123123");

        SOAPElement soapMessageHeaderCPAId = soapMessageHeaderElem.addChildElement("CPAId", "ns5");
        soapMessageHeaderCPAId.addTextNode("EY");

        SOAPElement soapMessageHeaderConversationId = soapMessageHeaderElem.addChildElement("ConversationId", "ns5");
        soapMessageHeaderConversationId.addTextNode("EYM0_53A363BD862690DC00FD98CECDB14979");

        SOAPElement soapMessageHeaderService = soapMessageHeaderElem.addChildElement("Service", "ns5");
        soapMessageHeaderService.addTextNode("AirFlifo");

        SOAPElement soapMessageHeaderAction = soapMessageHeaderElem.addChildElement("Action", "ns5");
        soapMessageHeaderAction.addTextNode("OTA_AirFlifoLLSRQ");

        SOAPElement soapMessageHeaderMessageData = soapMessageHeaderElem.addChildElement("MessageData", "ns5");
        SOAPElement soapMessageHeaderMessageDataMessageId = soapMessageHeaderMessageData.addChildElement("MessageId", "ns5");
        soapMessageHeaderMessageDataMessageId.addTextNode("mid:2013-07-30T20:44:45@eb2.com");

        SOAPElement soapMessageHeaderMessageDataTimestamp = soapMessageHeaderMessageData.addChildElement("Timestamp", "ns5");
        soapMessageHeaderMessageDataTimestamp.addTextNode("2013-07-30T20:44:45");

        SOAPElement soapSecurity = header.addChildElement("Security", "ns8");
        soapSecurity.addNamespaceDeclaration("ns4", "http://webservices.sabre.com/sabreXML/2003/07");
        soapSecurity.addNamespaceDeclaration("ns5", "http://www.ebxml.org/namespaces/messageHeader");
        soapSecurity.addNamespaceDeclaration("ns6", "http://www.w3.org/2000/09/xmldsig#");
        soapSecurity.addNamespaceDeclaration("ns7", "http://www.w3.org/1999/xlink");
        soapSecurity.addNamespaceDeclaration("ns8", "http://schemas.xmlsoap.org/ws/2002/12/secext");
        SOAPElement soapSecurityBinarySecurityToken = soapSecurity.addChildElement("BinarySecurityToken", "ns8");
        soapSecurityBinarySecurityToken.addTextNode(binarySecurityToken);

        /*
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	        <soap:Header>
		        <ns5:MessageHeader xmlns:ns4="http://webservices.sabre.com/sabreXML/2003/07" xmlns:ns5="http://www.ebxml.org/namespaces/messageHeader" xmlns:ns6="http://www.w3.org/2000/09/xmldsig#" xmlns:ns7="http://www.w3.org/1999/xlink" xmlns:ns8="http://schemas.xmlsoap.org/ws/2002/12/secext">
			        <ns5:From>
				        <ns5:PartyId>99999</ns5:PartyId>
			        </ns5:From>
			        <ns5:To>
				        <ns5:PartyId>123123</ns5:PartyId>
			        </ns5:To>
			        <ns5:CPAId>EY</ns5:CPAId>
			        <ns5:ConversationId>EYM0_53A363BD862690DC00FD98CECDB14979</ns5:ConversationId>
			        <ns5:Service>AirFlifo</ns5:Service>
			        <ns5:Action>OTA_AirFlifoLLSRQ</ns5:Action>
			        <ns5:MessageData>
				        <ns5:MessageId>mid:2013-07-26T20:44:45@eb2.com</ns5:MessageId>
				        <ns5:Timestamp>2013-07-26T20:44:45</ns5:Timestamp>
			        </ns5:MessageData>
		        </ns5:MessageHeader>
		        <ns8:Security xmlns:ns4="http://webservices.sabre.com/sabreXML/2003/07" xmlns:ns5="http://www.ebxml.org/namespaces/messageHeader" xmlns:ns6="http://www.w3.org/2000/09/xmldsig#" xmlns:ns7="http://www.w3.org/1999/xlink" xmlns:ns8="http://schemas.xmlsoap.org/ws/2002/12/secext">
			        <ns8:BinarySecurityToken>Shared/IDL:IceSess\/SessMgr:1\.0.IDL/Common/!ICESMS\/ACPCRTD!ICESMSLB\/CRT.LB!-3767252381629571069!497027!0</ns8:BinarySecurityToken>
		        </ns8:Security>
	        </soap:Header>
	        <soap:Body>
		        <ns4:OTA_AirFlifoRQ Version="2003A.TsabreXML1.5.1" xmlns:ns4="http://webservices.sabre.com/sabreXML/2003/07" xmlns:ns5="http://www.ebxml.org/namespaces/messageHeader" xmlns:ns6="http://www.w3.org/2000/09/xmldsig#" xmlns:ns7="http://www.w3.org/1999/xlink" xmlns:ns8="http://schemas.xmlsoap.org/ws/2002/12/secext">
			        <ns4:POS>
				        <ns4:Source/>
			        </ns4:POS>
			        <ns4:Airline Code="EY"/>
			        <ns4:FlightNumber>100</ns4:FlightNumber>
			        <ns4:DepartureDate>2013-07-27</ns4:DepartureDate>
		        </ns4:OTA_AirFlifoRQ>
	        </soap:Body>
        </soap:Envelope>
         */
         /*
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        //soapBody.addNamespaceDeclaration("ns4", "http://webservices.sabre.com/sabreXML/2003/07");

        SOAPElement soapBodyOTA_air = soapBody.addChildElement("OTA_AirFlifoRQ", "ns4");
        soapBodyOTA_air.addAttribute(QName.valueOf("Version"), "2003A.TsabreXML1.5.1");
        soapBodyOTA_air.addNamespaceDeclaration("ns4", "http://webservices.sabre.com/sabreXML/2003/07");
        soapBodyOTA_air.addNamespaceDeclaration("ns5", "http://www.ebxml.org/namespaces/messageHeader");
        soapBodyOTA_air.addNamespaceDeclaration("ns6", "http://www.w3.org/2000/09/xmldsig#");
        soapBodyOTA_air.addNamespaceDeclaration("ns7", "http://www.w3.org/1999/xlink");
        soapBodyOTA_air.addNamespaceDeclaration("ns8", "http://schemas.xmlsoap.org/ws/2002/12/secext");


        SOAPElement soapBodyOTAAirlinePOS = soapBodyOTA_air.addChildElement("POS", "ns4");
        SOAPElement soapBodyOTAAirlinePOSSource = soapBodyOTAAirlinePOS.addChildElement("Source", "ns4");

        SOAPElement soapBodyOTAAirline = soapBodyOTA_air.addChildElement("Airline", "ns4");
        soapBodyOTAAirline.addAttribute(QName.valueOf("Code"), airline);

        SOAPElement soapBodyOTAFlightNumber = soapBodyOTA_air.addChildElement("FlightNumber", "ns4");
        soapBodyOTAFlightNumber.addTextNode(flightNumber);

        SOAPElement soapBodyOTADepartureDate = soapBodyOTA_air.addChildElement("DepartureDate", "ns4");
        soapBodyOTADepartureDate.addTextNode("2013-07-31");

        soapMessage.saveChanges();

        // Print the request message
        System.out.print("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }   */

}
