package com.sabre.PushNotificationsWeb;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/21/13
 * Time: 3:08 PM
 */
//@WebServlet("/send")
public class Send extends HttpServlet {
    private static Logger log = Logger.getRootLogger();
    private static final String SIMPLE_SEND_ACTION = "simple";
    private static final String SIMPLE_SEND_TITLE = "Received a message";
    //private ArrayList<String> theIds = new ArrayList<String>();
    //private Log myLog = Log.getInstance();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Log statement here
        log.info("Sending...");

        ArrayList<String> theIds = new ArrayList<String>();


        String message = request.getParameter("notif_text");
        String[] users = request.getParameterValues("user[]");

        // translate from array to arraylist for GCM server
        if(users != null) {
            for(int i = 0; i < users.length; i ++) {
                log.info("user: " + users[i] + "\nmsg: " + message);
                theIds.add(users[i]);
            }
        }

        if (request.getParameter("send_notification") != null) {
            log.info("Sending: " + message);
            PushServer.sendMessage(theIds, SIMPLE_SEND_ACTION, SIMPLE_SEND_TITLE, message, null);
        }

    }



}
