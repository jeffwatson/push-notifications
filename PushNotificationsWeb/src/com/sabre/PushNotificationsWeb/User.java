package com.sabre.PushNotificationsWeb;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 6/24/13
 * Time: 12:20 PM
 */
public class User {
    private static final Logger log = Logger.getRootLogger();

    private String username;
    private String reg_id;
    private String group;

    public User(String u, String id) {
        this.username = u;
        this.reg_id = id;

        writeUserToDb(this);
    }

    public void setUsername(String u) {
        this.username = u;
    }
    public void setReg_id(String id) {
        this.reg_id = id;
    }
    public void setGroup(String g) {
        this.group = g;
    }

    public String getUsername() {
        return this.username;
    }
    public String getReg_id() {
        return this.reg_id;
    }
    public String getGroup() {
        return this.group;
    }

    @Override
    public String toString() {
        return "USERNAME= " + this.getUsername() + " ID= " + this.getReg_id();
    }

    public static Map readDB() {
        Map<String, ArrayList<String>> dbMap = new HashMap<String, ArrayList<String>>();
        ArrayList<String> ids = new ArrayList<String>();
        ArrayList<String> names = new ArrayList<String>();

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT gcm_id, name FROM users;" );
            while ( rs.next() ) {
                ids.add(rs.getString("gcm_id"));
                names.add(rs.getString("name"));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            log.error("Error getting users from db.", e);
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            //System.exit(0);
        }

        dbMap.put("ids", ids);
        dbMap.put("names", names);
        return dbMap;
    }

    private void writeUserToDb(User u) {
        log.info("Writing user: " + u.getUsername());
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "INSERT INTO users VALUES (null, \'"+u.getReg_id() +"\', \'" + u.getUsername() + "\', \'\');";
            stmt.executeUpdate(sql);

            stmt.close();
            c.commit();
            c.close();
        } catch ( Exception e ) {
            log.error("error adding user.", e);
            //System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            //System.exit(0);
        }

    }
}
