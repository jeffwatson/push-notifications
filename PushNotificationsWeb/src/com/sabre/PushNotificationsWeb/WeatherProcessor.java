package com.sabre.PushNotificationsWeb;

import org.apache.camel.Exchange;


import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.net.ssl.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;

/**
 * Package: com.sabre.PushNotificationsWeb
 * Project: PushNotifications
 * <p/>
 * Author: Jeff Watson <Jeff.Watson@sabre.com>
 * Date: 8/13/13
 * Time: 4:36 PM
 */

public class WeatherProcessor {
    private static final Logger log = Logger.getRootLogger();
    private static final double TEMPERATURE_THRESHOLD = 10; // 20 degree or greater difference sends a notification.
    private static final String WEATHER_BASE_URL = "http://openweathermap.org/data/2.5/weather"; //append city to this
    private static final String WEATHER_MODE = "mode=xml";
    private static final String WEATHER_API_KEY = "APPID=5b0cc6f6b3d8b8b37a3ae1b179ec6acb";
    private static final String WEATHER_ACTION = "weather";
    private static final String WEATHER_ALERT_TITLE = "Weather Alert";
    private static final String WEATHER_ALERT_SUBTITLE = "Destination Temperature is ";
    private static final long DATA_EXPIRED_THRESHOLD = 1000 * 60 * 60; // 1 hour

    /**
     * This method will handle the changes in weather and notifications.
     * It sends a request based on the origin and destination airports, compares the temperatures,
     * and if the difference is higher than the threshold, sends a notification to users on that flight.
     *
     * @param exchange the information coming from the table through camel
     */

    public void weatherWatcher(Exchange exchange) {
        log.info("Weather Watcher Received Exchange: " + exchange.toString());
        String result = exchange.getIn().getBody(String.class);

        String o_code = result.substring(result.indexOf("o_code="), result.indexOf(',', result.indexOf("o_code=")));
        o_code = o_code.replaceAll("o_code=", "");
        String d_code = result.substring(result.indexOf("d_code="), result.indexOf(',', result.indexOf("d_code=")));
        d_code = d_code.replaceAll("d_code=", "");

        String airline_code = result.substring(result.indexOf("airline_code="), result.indexOf(',', result.indexOf("airline_code=")));
        String flight_number = result.substring(result.indexOf("flight_number="), result.indexOf(',', result.indexOf("flight_number=")));
        String flight_date = result.substring(result.indexOf("flight_date="), result.indexOf(',', result.indexOf("flight_date=")));

        airline_code = airline_code.replaceAll("airline_code=", "");
        flight_number = flight_number.replaceAll("flight_number=", "");
        flight_date = flight_date.replaceAll("flight_date=", "");

        String oCity;
        String dCity;
        String requestURL;

        HashMap<String, String> oWeatherValues;
        HashMap<String, String> dWeatherValues;

        String oCurrentTemp;
        String dCurrentTemp;

        if(weatherDbContains(o_code)) {
            // get the values from the table
            oWeatherValues = getWeatherValues(o_code);
            // compare time to now
            if(System.currentTimeMillis() - Long.valueOf(oWeatherValues.get("timestamp")) >= DATA_EXPIRED_THRESHOLD) {
                // refresh the data
                requestURL = WEATHER_BASE_URL + oWeatherValues.get("city_name") + WEATHER_API_KEY;
                oWeatherValues = doHttpURLConnection(requestURL);
                oWeatherValues.put("code", o_code);
                oCurrentTemp = oWeatherValues.get("current_temp");
                //oWeatherValues.put("temp", oTemp);
                // update the table
                updateWeatherInDb(oWeatherValues);
            } else {
                // use this data
                oCurrentTemp = oWeatherValues.get("current_temp");
            }
        } else {
            // get the city name from the file
            oCity = getCityNameFromPropertiesFile(o_code);
            oCity = oCity.substring(0, oCity.indexOf(",")).replaceAll("International", "").replaceAll("Airport", "").replaceAll(" ", "%20");

            // fire off the HttpURLConnection request
            requestURL = WEATHER_BASE_URL + oCity + WEATHER_API_KEY;
            oWeatherValues = doHttpURLConnection(requestURL);
            oCurrentTemp = oWeatherValues.get("current_temp");

            if(oCurrentTemp != null) {
                //HashMap<String, String> values = new HashMap<String, String>();
                oWeatherValues.put("code", o_code);
                oWeatherValues.put("city_name", oCity);
                //values.put("temp", oTemp);

                writeWeatherToDB(oWeatherValues);
            } else {
                // change to o_code and try again
                requestURL = WEATHER_BASE_URL + o_code + WEATHER_API_KEY;
                oWeatherValues = doHttpURLConnection(requestURL);

                // now they *shouldn't* be null, so save the values
                if(oWeatherValues != null) {
                    //HashMap<String, String> values = new HashMap<String, String>();
                    oWeatherValues.put("code", o_code);
                    oWeatherValues.put("city_name", o_code);
                    //values.put("temp", oTemp);

                    writeWeatherToDB(oWeatherValues);
                } else {
                    System.out.println("Unable to find a suitable city name for code: " + o_code);
                }
            }
        }

        if(weatherDbContains(d_code)) {
            // get the values from the table
            dWeatherValues = getWeatherValues(d_code);
            // compare time to now
            if(System.currentTimeMillis() - Long.valueOf(dWeatherValues.get("timestamp")) >= DATA_EXPIRED_THRESHOLD) {
                // refresh the data
                requestURL = WEATHER_BASE_URL + dWeatherValues.get("city_name") + WEATHER_API_KEY;
                dWeatherValues = doHttpURLConnection(requestURL);
                dWeatherValues.put("code", d_code);
                dCurrentTemp = dWeatherValues.get("current_temp");
                //dWeatherValues.put("temp", dTemp);
                // update the table
                updateWeatherInDb(dWeatherValues);
            } else {
                // use this data
                dCurrentTemp = dWeatherValues.get("current_temp");
            }
        } else {
            // get the city name from the properties file
            dCity = getCityNameFromPropertiesFile(d_code);
            dCity = dCity.substring(0, dCity.indexOf(",")).replaceAll("International", "").replaceAll("Airport", "").replaceAll(" ", "%20");
            // fire off the HttpURLConnection request
            requestURL = WEATHER_BASE_URL + dCity + WEATHER_API_KEY;
            dWeatherValues = doHttpURLConnection(requestURL);
            dCurrentTemp = dWeatherValues.get("current_temp");


            if(dCurrentTemp != null) {
                //HashMap<String, String> values = new HashMap<String, String>();
                dWeatherValues.put("code", d_code);
                dWeatherValues.put("city_name", dCity);
                //values.put("temp", dTemp);

                writeWeatherToDB(dWeatherValues);
            } else {
                // change to o_code and try again
                requestURL = WEATHER_BASE_URL + d_code + WEATHER_API_KEY;
                dWeatherValues = doHttpURLConnection(requestURL);
                dCurrentTemp = dWeatherValues.get("current_temp");

                // now they *shouldn't* be null, so save the values
                if(dWeatherValues != null) {
                    //HashMap<String, String> values = new HashMap<String, String>();
                    dWeatherValues.put("code", d_code);
                    dWeatherValues.put("city_name", d_code);
                    //values.put("temp", dTemp);

                    writeWeatherToDB(dWeatherValues);
                } else {
                    System.out.println("Unable to find a suitable city name for code: " + o_code);
                }
            }
        }

        //System.out.println("O code = " + o_code + ", D city = " + dCity);
        System.out.println("Otemp = " + oCurrentTemp + ", Dtemp = " + dCurrentTemp);

        // compare the values
        if(Math.abs(Double.valueOf(oCurrentTemp) - Double.valueOf(dCurrentTemp)) >= TEMPERATURE_THRESHOLD) {
            System.out.println("Temperature is greater than threshold, Sending message.");
            HashMap<String, String> values = new HashMap<String, String>();

            // TODO I think this is going to change.
            values.put("airline_code", airline_code);
            values.put("flight_number", flight_number);
            values.put("d_temp", dCurrentTemp);
            values.put("flight_date", flight_date);

            messageUsersOnFlight(values);
        }
    }

    private static HashMap<String,String> getTemperatureFromResponse(String source) {
        HashMap<String, String> values = new HashMap<String, String>();

        String value = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(source)));
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();

            XPathExpression expr = xpath.compile("/current/temperature/@min");
            value = (String) expr.evaluate(doc, XPathConstants.STRING);
            values.put("min_temp", value);

            expr = xpath.compile("/current/temperature/@max");
            value = (String) expr.evaluate(doc, XPathConstants.STRING);
            values.put("max_temp", value);

            expr = xpath.compile("/current/temperature/@value");
            value = (String) expr.evaluate(doc, XPathConstants.STRING);
            values.put("current_temp", value);

            expr = xpath.compile("/current/weather/@icon");
            value = (String) expr.evaluate(doc, XPathConstants.STRING);
            values.put("condition",value);


        } catch (Exception e) {
            log.error("Unable to parse String into Document.", e);
        }

        return values;
    }

    private boolean messageUsersOnFlight(HashMap<String, String> values) {
        boolean xmlCreated = false;

        String flight_number = values.get("flight_number");
        String airline_code = values.get("airline_code");
        String d_temp = values.get("d_temp");
        String flight_date = values.get("flight_date");

        // users db contains airline code + number in one field
        String compositeAirlineNumber = airline_code + flight_number + " " + flight_date;

        //System.out.println("Finding users on flight: "+ flightNumber);
        log.info("finding users on flight " + flight_number);
        System.out.println("Messaging users on flight " + flight_number);

        Connection c;
        Statement stmt;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM users WHERE next_flight LIKE\'%" + compositeAirlineNumber + "%\';" );
            while ( rs.next() ) {
                String gcm_id = rs.getString("gcm_id");

                // Create the xml file to be added to camel's directory.
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

                // root elements
                Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("notification");
                doc.appendChild(rootElement);

                // id elements
                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(gcm_id));
                rootElement.appendChild(id);

                // action elements
                Element action = doc.createElement("action");
                action.appendChild(doc.createTextNode(WEATHER_ACTION));
                rootElement.appendChild(action);

                // title elements
                Element title = doc.createElement("title");
                title.appendChild(doc.createTextNode(WEATHER_ALERT_TITLE));
                rootElement.appendChild(title);

                // subtitle elements
                Element subtitle = doc.createElement("subtitle");
                subtitle.appendChild(doc.createTextNode(WEATHER_ALERT_SUBTITLE + d_temp));
                rootElement.appendChild(subtitle);

                // data elements, a JSON object to decode on the device side
                //Element data = doc.createElement("data");
                //data.appendChild(doc.createTextNode(generateJSONPayload(values)));
                //rootElement.appendChild(data);

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(Log.ROOT_OF_PROJECT + "pending/" + System.currentTimeMillis() + ".xml"));

                transformer.transform(source, result);
                xmlCreated = true;
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );

        }
        return xmlCreated;
    }

    private static void writeWeatherToDB(HashMap<String, String> values) {
        String code = values.get("code");
        String city_name = values.get("city_name");
        String current_temp = values.get("current_temp");
        String min_temp = values.get("min_temp");
        String max_temp = values.get("max_temp");
        String condition = values.get("condition");

        log.info("Writing weather for: " + code);
        Connection c;
        Statement stmt;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "INSERT INTO weather VALUES (\'"+code +"\', \'" + city_name + "\', \'" + current_temp
                    +"\',\'"+ min_temp +"\',\'" + max_temp +"\', \'" + condition +"\', \'" + System.currentTimeMillis()+"\');";
            stmt.executeUpdate(sql);

            stmt.close();
            c.commit();
            c.close();
        } catch ( Exception e ) {
            log.error("Error adding weather.", e);
        }
    }

    public static boolean weatherDbContains(String code) {
        boolean contained = false;
        String containedCode;

        System.out.println("Checking if weather db contains a value for " + code);
        Connection c;
        Statement stmt;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "SELECT * FROM weather WHERE code=\'" + code + "\';";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                containedCode = rs.getString("code");
                log.info("Got code of "+ code);
                if(containedCode.equals(code)) {
                    contained = true;
                }
            }

            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            log.error("Error checking if weather is in db.", e);
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        return contained;
    }

    private void updateWeatherInDb(HashMap<String, String> values) {
        System.out.println("Updating " + values.get("code") + " in weather db.");

        Connection c;
        Statement stmt;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            int num = stmt.executeUpdate( "UPDATE weather SET min_temp=\'" + values.get("min_temp") +
                    "\', max_temp=\'" + values.get("max_temp") + "\', current_temp=\'"+ values.get("current_temp") + "\'," +
                    "condition=\'" + values.get("condition") + "\', timestamp="+System.currentTimeMillis() +
                    " WHERE code=\'" + values.get("code") + "\';" );
            log.info("updated " + num + " rows");
            System.out.println("updated "+ num + " rows");

            c.commit();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            log.error("Error while updating flight_data", e);
        }

    }

    public static HashMap<String, String> getWeatherValues(String code) {
        HashMap<String, String> values = new HashMap<String, String>();
        Connection c;
        Statement stmt;

        if(weatherDbContains(code)) {

            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:" + Log.ROOT_OF_PROJECT + "database/push_notifications.db");
                c.setAutoCommit(false);

                stmt = c.createStatement();
                String sql = "SELECT * FROM weather WHERE code=\'" + code + "\';";

                ResultSet rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    values.put("code", code);
                    values.put("city_name", rs.getString("city_name"));
                    values.put("min_temp", rs.getString("min_temp"));
                    values.put("max_temp", rs.getString("max_temp"));
                    values.put("current_temp", rs.getString("current_temp"));
                    values.put("condition", rs.getString("condition"));
                    values.put("timestamp", rs.getString("timestamp"));
                }

                rs.close();
                stmt.close();
                c.close();
            } catch ( Exception e ) {
                log.error("Error checking if weather is in db.", e);
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            }

        } else {
            // get the city name from the file
            String City = getCityNameFromPropertiesFile(code);
            City = City.substring(0, City.indexOf(",")).replaceAll("International", "").replaceAll("Airport", "").replaceAll(" ", "%20");

            // fire off the HttpURLConnection request
            String requestURL = City;
            HashMap<String, String> WeatherValues = doHttpURLConnection(requestURL);
            String CurrentTemp = WeatherValues.get("current_temp");

            if(CurrentTemp != null) {
                //HashMap<String, String> values = new HashMap<String, String>();
                WeatherValues.put("code", code);
                WeatherValues.put("city_name", City);
                //values.put("temp", oTemp);

                values = WeatherValues;

                writeWeatherToDB(WeatherValues);
            } else {
                // change to o_code and try again
                requestURL = code;
                WeatherValues = doHttpURLConnection(requestURL);

                // now they *shouldn't* be null, so save the values
                if(WeatherValues != null) {
                    //HashMap<String, String> values = new HashMap<String, String>();
                    WeatherValues.put("code", code);
                    WeatherValues.put("city_name", code);
                    //values.put("temp", oTemp);

                    values = WeatherValues;

                    writeWeatherToDB(WeatherValues);
                } else {
                    System.out.println("Unable to find a suitable city name for code: " + code);
                }
            }

        }

        return values;
    }

    //TODO get this working on the server!
    private static HashMap<String, String> doHttpURLConnection(String requestCity) {
        /*System.setProperty("http.proxyHost", "tulsa-proxy.sabre.com");
        System.setProperty("http.proxyPort", "80");
        //System.setProperty("http.proxyUser", "sg0218937");
        //System.setProperty("http.proxyPassword", "trinity2");
        /*Authenticator.setDefault(
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                "sg0218937", "trinity2".toCharArray());
                    }
                }
        );   */
        //String userPassword = "sq0218937" + ":" + "trinity2";
        //String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
        //URLConnection uc = url.openConnection();
        //uc.setRequestProperty("Authorization", "Basic " + encoding);
        //uc.connect();

        //String temperature = null;
        int responseCode = 0;
        InputStream response;
        StringBuilder sb = new StringBuilder();
        String requestURL = WEATHER_BASE_URL + "?q=" + requestCity +  "&" + WEATHER_API_KEY + "&" + WEATHER_MODE;
        log.info("Request URL = " + requestURL);
        HttpURLConnection conn = (HttpURLConnection) getURLConnection(requestURL, true);


        try {
            //conn.setInstanceFollowRedirects(true);
//            conn = (HttpURLConnection) getURLConnection(requestURL, true);
            //conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.connect();

            conn.setInstanceFollowRedirects(true);

            responseCode =  conn.getResponseCode();

            response = conn.getInputStream();
            InputStreamReader is = new InputStreamReader(response);
            BufferedReader br = new BufferedReader(is);
            String read = br.readLine();

            while(read != null) {
                sb.append(read);
                read = br.readLine();
            }

            return getTemperatureFromResponse(sb.toString());
        } catch (Exception e) {
            log.error("Unable to connect to weather service url " + requestURL + "\nResponse code=" + responseCode, e);

            response = conn.getErrorStream();
            InputStreamReader is = new InputStreamReader(response);

            BufferedReader br = new BufferedReader(is);
            try {
                String read = br.readLine();

                while(read != null) {
                    sb.append(read);
                    read = br.readLine();
                }
            } catch (IOException ioe) {
                log.error("Unable to print error stream :( ", ioe);
            }
            log.info("error message= " + sb.toString());
        }
        /*String result = null;
        try {
            result = docURLAction(requestURL);

            return getTemperatureFromResponse(result);
        } catch (Exception e) {
            log.error("Unable to connect to weather service url " + requestURL, e);
        }   */

        return null;
    }
    /*
    // TODO get this working!!!!
    public static String doHttpClientConnection(String desiredCity) {

        String mResponse = null;

        try {
            HttpHost proxy = new HttpHost("tulsa-proxy.sabre.com", 80);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);

            HttpHost targetHost = new HttpHost(WEATHER_BASE_URL + "?" + WEATHER_MODE + "&"
                    + "q=" + desiredCity + "&" + WEATHER_API_KEY, 80, "http");
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                    new UsernamePasswordCredentials("sg0218937", "trinity2"));
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setDefaultCredentialsProvider(credsProvider)
                    .setRoutePlanner(routePlanner).build();
            try {

                // Create AuthCache instance
                AuthCache authCache = new BasicAuthCache();
                // Generate BASIC scheme object and add it to the local
                // auth cache
                BasicScheme basicAuth = new BasicScheme();
                authCache.put(targetHost, basicAuth);

                // Add AuthCache to the execution context
                HttpClientContext localContext = HttpClientContext.create();
                localContext.setAuthCache(authCache);

                HttpGet httpget = new HttpGet("/");

                System.out.println("executing request: " + httpget.getRequestLine());
                log.info("executing request: " + httpget.getRequestLine());
                System.out.println("to target: " + targetHost);
                log.info("to target: " + targetHost);

                for (int i = 0; i < 3; i++) {
                    CloseableHttpResponse response = httpclient.execute(targetHost, httpget, localContext);
                    try {
                        HttpEntity entity = response.getEntity();

                        System.out.println("----------------------------------------");
                        System.out.println(response.getStatusLine());
                        log.info(response.getStatusLine());
                        if (entity != null) {
                            System.out.println("Response content length: " + entity.getContentLength());
                            log.info("Response content length: " + entity.getContentLength());
                        }
                        EntityUtils.consume(entity);
                    } finally {
                        response.close();
                    }
                }
            } finally {
                httpclient.close();
            }

        /*
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope("localhost", 8080),
                    new UsernamePasswordCredentials("username", "password"));
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setDefaultCredentialsProvider(credsProvider).build();
            try {
                HttpHost targetHost = new HttpHost("www.verisign.com", 443, "https");
                HttpHost proxy = new HttpHost("localhost", 8080);

                RequestConfig config = RequestConfig.custom()
                        .setProxy(proxy)
                        .build();
                HttpGet httpget = new HttpGet("/");
                httpget.setConfig(config);

                System.out.println("executing request: " + httpget.getRequestLine());
                System.out.println("via proxy: " + proxy);
                System.out.println("to target: " + targetHost);

                CloseableHttpResponse response = httpclient.execute(targetHost, httpget);
                try {
                    HttpEntity entity = response.getEntity();

                    System.out.println("----------------------------------------");
                    System.out.println(response.getStatusLine());
                    if (entity != null) {
                        System.out.println("Response content length: " + entity.getContentLength());
                    }
                    EntityUtils.consume(entity);
                } finally {
                    response.close();
                }
            } finally {
                httpclient.close();
            }    */
        /*
        } catch (Exception e) {
            log.error("Caught exception in HttpClient method.", e);
        }

        return mResponse;
    }   */

    /**
     * Returns the output from the given baseURL.
     *
     * @param desiredCity City were getting info for we're connecting to
     * @return HTML returned by source
     * @throws Exception
     */
    // TODO this doesn't work either... WTF?
    // connection is always refused...
    public static String docURLAction(String desiredCity) throws Exception {
        String html;
        ProcessBuilder p;

        p = new ProcessBuilder("whoami");
        log.info(p.command().toString());
        final Process shell2 = p.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        log.info(stringBuilder.toString());

        // command template:
        //curl -G http://openweathermap.org/data/2.5/weather -d 'q=John%20F%20Kennedy%20' -d
        // 'APPID=5b0cc6f6b3d8b8b37a3ae1b179ec6acb' -d 'mode=xml' --proxy-basic -x tulsa-proxy.sabre.com:80
        // --proxy-user sg0218937:trinity2 -c /tmp/cookie-jar.txt -m 5 -v

        p = new ProcessBuilder("curl", "-G", WEATHER_BASE_URL, "-d", "\'q=" + desiredCity + "\'", "-d", "\'" + WEATHER_API_KEY + "\'",
                "-d", "\'" + WEATHER_MODE + "\'", "--proxy-basic", "-v",
                "--proxy", "tulsa-proxy.sabre.com:80", "--proxy-user", "sg0218937:trinity2", "-c", "/tmp/cookie-jar.txt", "-m", "5");

        final Process shell;

        log.info(p.command().toString());
        shell = p.start();

        reader = new BufferedReader(new InputStreamReader(shell.getInputStream()));
        stringBuilder = new StringBuilder();

        //line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        html = stringBuilder.toString();

        if (html.equals("")) {
            reader = new BufferedReader(new InputStreamReader(shell.getErrorStream()));
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

            log.info(stringBuilder.toString());
        }

        log.info("CURL HTML = " + html);
        return html;
    }

    private static String getCityNameFromPropertiesFile(String code) {
        String cityName = null;

        Properties cities = new Properties();
        try {
            cities.load(new FileInputStream(Log.ROOT_OF_PROJECT + "cities/cities.properties"));
            cityName = cities.getProperty("City." + code);
        } catch (IOException e) {
            log.error("Unable to read cities properties file.", e);
        }

        return cityName;
    }

    public static URLConnection getURLConnection(String paramURL, boolean withProxy)
    {
        URLConnection conn = null;
        try
        {
            URL url = new URL(paramURL); // "https://wl16-int.sabresonicweb.com/SSW2010/EYM0/landing.html"

            if (null != paramURL && paramURL.contains("https"))
            {   /* This shouldn't happen.
                // configure the SSLContext with a TrustManager
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
                SSLContext.setDefault(ctx);

                if (withProxy)
                {
                    log.info("CONNECTING HTTPS THROUGH PROXY TO --> " + paramURL);

                    System.getProperties().put("https.proxySet", "true");
                    System.getProperties().put("https.proxyHost", "www-ad-proxy.sabre.com");
                    System.getProperties().put("https.proxyPort", "80");
                    System.getProperties().put("https.proxyUserName", "sg0920208");
                    System.getProperties().put("https.proxyPassWord", "abcd1234");

                    conn = url.openConnection();
                    ((HttpsURLConnection) conn).setHostnameVerifier(new HostnameVerifier()
                    {
                        @Override
                        public boolean verify(String hostname, SSLSession session)
                        {
                            return false;
                        }
                    });

                    Authenticator authenticator = new Authenticator()
                    {

                        @Override
                        public PasswordAuthentication getPasswordAuthentication()
                        {
                            return (new PasswordAuthentication("sg0920208", "abcd1234".toCharArray()));
                        }
                    };
                    Authenticator.setDefault(authenticator);

                }
                else
                {
                    log.info("CONNECTING HTTPS WITHOUT PROXY TO --> " + paramURL);
                    System.setProperty("https.proxySet", "false");
                    conn = url.openConnection();
                }   */
            }
            else
            {

                if (withProxy)
                {
                    log.info("CONNECTING HTTP THROUGH PROXY TO --> " + paramURL);

                    System.getProperties().put("http.proxySet", "true");
                    System.getProperties().put("http.proxyHost", "outbound-proxy.cert.sabre.com");
                    System.getProperties().put("http.proxyPort", "80");
                    System.getProperties().put("http.proxyUserName", "sg0920208");
                    System.getProperties().put("http.proxyPassWord", "abcd1234");

                    Authenticator authenticator = new Authenticator()
                    {

                        @Override
                        public PasswordAuthentication getPasswordAuthentication()
                        {
                            return (new PasswordAuthentication("sg0920208", "abcd1234".toCharArray()));
                        }
                    };
                    Authenticator.setDefault(authenticator);

                    conn = url.openConnection();
                }
                else
                {
                    log.info("CONNECTING HTTP WITHOUT PROXY TO --> " + paramURL);
                    System.setProperty("http.proxySet", "false");
                    conn = url.openConnection();
                }
            }

        }
        catch (IOException ioe) {
            log.error(ioe.getMessage());
        } /*catch (KeyManagementException ioe) {
            log.error(ioe.getMessage());
        } catch (NoSuchAlgorithmException ioe) {
            log.error(ioe.getMessage());
        }   */

        return conn;
    }

}
