CREATE TABLE users (id INTEGER PRIMARY KEY, gcm_id VARCHAR(100), name VARCHAR(100), next_flight VARCHAR(100));
CREATE TABLE weather (code VARCHAR(100) PRIMARY KEY, city_name VARCHAR(100), current_temp REAL, min_temp REAL, max_temp REAL, condition VARCHAR(100), timestamp
INTEGER);
CREATE TABLE flights (id INTEGER PRIMARY KEY, flight_date DATE, airline_code VARCHAR(100), flight_number VARCHAR(100), o_context VARCHAR(100), o_code VARCHAR(100), o_gate VARCHAR(100), o_scheduled VARCHAR(100), o_scheduled_ind VARCHAR(100), o_terminal VARCHAR(100), d_context VARCHAR(100), d_code VARCHAR(100), d_scheduled VARCHAR(100), d_scheduled_ind VARCHAR(100), day_ind VARCHAR(100));
CREATE TABLE NOTIFICATIONS (id INTEGER PRIMARY KEY, userID INTEGER, routeId INTEGER, sent INTEGER, action TEXT, title TEXT, subtitle TEXT, data TEXT);
CREATE TABLE routes (id INTEGER PRIMARY KEY, value VARCHAR, start_date DATETIME, stop_date DATETIME);