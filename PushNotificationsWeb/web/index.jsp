<%@ page import="com.sabre.PushNotificationsWeb.User" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>

<%--
* Project: PushNotificationsWeb
*
* Author: Jeff Watson <Jeff.Watson@sabre.com>
* Date: 6/20/2013
* Time: 9:32 AM
*/--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Push Notifications Web App</title>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <script src="js/bootstrap.min.js" type="text/javascript"></script>

    <!-- jQuery -->
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
    <!--<script src="js/jquery-ui.js" type="text/javascript"></script>     -->

    <!-- TimePicker -->
    <!--<script src="js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-timepicker.min.css"/>        -->

    <!-- selectpicker -->
    <script src="js/bootstrap-select.js" type="text/javascript"></script>
    <link href="css/bootstrap-select.min.css" rel="stylesheet"/>

    <!-- Bootstrap Multiselect -->
    <script src="js/bootstrap-multiselect.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-multiselect.css"/>

    <!-- Bootstrap DateTime Picker -->
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="css/datetimepicker.css"/>

    <!-- CSS -->
    <link href="css/main.css" rel="stylesheet" media="screen">
</head>
<body>
<script type="text/javascript">
$(document).ready( function() {
    // variable to hold request
    var request;
    // bind to the submit event of our form
    $('#send_btn').click(function(event){
        // abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $('#send');
        // let's select and cache all the fields
        var $inputs = $form.find("input, button, select, textarea");
        // serialize the data in the form
        var serializedData = $form.serialize();

        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);

        // fire off the request to /send
        request = $.ajax({
            url: "/PushNotificationsWeb_war_exploded/send",
            type: "post",
            data: serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // log a message to the console
            console.log("Hooray, it worked!");
            appendLast();
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                    "The following error occured: "+
                            textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $inputs.prop("disabled", false);
        });

        // prevent default posting of form
        event.preventDefault();
    });

    $('#new_route_submit').click(function(event){
        // abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $('.create_new_route');
        // let's select and cache all the fields
        var $inputs = $form.find("button, select[name='type'], select, textarea, input.time_value, input.degrees, .date,  .selectedAirports input, #notif_subtitle, #cal_start, #cal_end");
        // serialize the data in the form
        var serializedData = $inputs.serialize();

        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);

        // fire off the request to /new_route
        request = $.ajax({
            url: "./new_route",
            type: "post",
            data: serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // log a message to the console
            console.log("Hooray, it worked!");


            $('#sentItems').append("<div>" + response + "</div>");

            $('#new_route_element_select').slideUp('slow', function() {
                $('.route_element_location:not(:last)').remove();
                $('.route_element_weather:not(:last)').remove();
                $('.route_element_time:not(:last)').remove();
                $('.route_element_before_takeoff:not(:last)').remove();
                $('.route_element_single_condition:not(:last)').remove();
                $('.new_route_element_select').hide();
                $('.new_route_controls').css('display','none');

                $('#notif_subtitle').val('');
                $('#cal_start').val('');
                $('#cal_end').val('');
            });
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                    "The following error occured: "+
                            textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $inputs.prop("disabled", false);
        });

        // prevent default posting of form
        event.preventDefault();
    });
});

function changeTimeLayout(thisItem) {
    var value = $(thisItem).val();
    console.log("changing time layout to " + value);

    var compareOptions = { "Less than":"<" , "Greater than":">"};
    var beforeAfter = {"Before":"<", "After":">"};
    var parent = $(thisItem).parents(".route_element");

    var select = parent.find($('.time_condition'));
    var input = parent.find($('.time_value'));

    //activateSelectPickers();

    input.val('');

    // remove the old options, append the new ones.
    select.find('option').remove();

    if(value == "delay" || value == "duration") {
        $.each(compareOptions, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });

        input.attr("placeholder", "Minutes");

    } else if(value == "o_scheduled" || value == "d_scheduled") {
        $.each(beforeAfter, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });

        input.attr("placeholder", "Time");

        console.log("adding datetimepickers");
        input.datetimepicker({
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 0,
            minuteStep: 1
        });
    }

    $('#time_selectpicker').selectpicker('refresh');
}

/*function activateSelectPickers() {
 console.log("Activating Select Pickers!");

 $('.selectpicker').selectpicker({
 size: false
 });

 /*$('.selectpicker').not("[style='display: none']").selectpicker({
 size: false
 }); */
/*}    */

/*function activateButtonHandlers() {

 var $buttons = $('.btn.dropdown-toggle');

 $buttons.click(function() {
 console.log("from button handler!");
 var $parent = $(this).parents('div.btn-group');
 var $isOpen = $parent.hasClass('open');

 if($isOpen) {
 $parent.removeClass('open');
 } else {
 $parent.addClass('open');
 }
 });

 $buttons.blur(function () {
 console.log("lost focus!");
 //if(this.hasClass('open')) {
 //    alert("Lost focus!");
 // }

 });

 }      */

/*function makeSortable() {
 $( ".selectedAirports, .deselectedAirports" ).sortable({
 connectWith: ".selectedAirportsItem",
 dropOnEmpty: "true"
 }).disableSelection();
 }   */
/*$('.multiselect').click(function () {
 alert("clicked!");
 this.multiselect('rebuild');

 }); */

function showConditions() {
    $('#new_route_element_select').css('display','block').show();
    var firstElement = $('#route_element_location').clone().removeAttr('id');
    var controls = $('#new_route_controls');
    var container = $('#if-container');

    controls.css('display','block');
    firstElement.css('display','block');

    container.append(firstElement).hide().slideDown('slow');
    controls.css('display','block').hide().slideDown('slow');



    console.log("adding multiselect");
    firstElement.find('.multiselect').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 250
    });

    console.log("adding datetimepickers");
    $('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 0,
        minuteStep: 1
    });

    firstElement.children('.selectpicker').selectpicker({
        size: false
    });

    firstElement.find('.btn.dropdown-toggle').click(function() {
        console.log("from button handler!");
        var $parent = $(this).parents('div.btn-group');
        var $isOpen = $parent.hasClass('open');

        if($isOpen) {
            $parent.removeClass('open');
        } else {
            $parent.addClass('open');
        }
    });

    /*firstElement.find('.btn.dropdown-toggle').focusout(function () {
     console.log("lost focus! to ");

     var $parent = $(this).parents('div.btn-group');
     var $isOpen = $parent.hasClass('open');

     if($isOpen && document.activeElement != $parent.find()) {
     $parent.removeClass('open');
     } else {
     $parent.addClass('open');
     }
     //if(this.hasClass('open')) {
     //    alert("Lost focus!");
     // }

     });  */

    $('#then_send').selectpicker({
        size: false
    });

    $('#notify').selectpicker({
        size: false
    });

    $('#add_variable').selectpicker({
        size:false
    });

    controls.find('.btn.dropdown-toggle').click(function() {
        console.log("from button handler!");
        var $parent = $(this).parents('div.btn-group');
        var $isOpen = $parent.hasClass('open');

        if($isOpen) {
            $parent.removeClass('open');
        } else {
            $parent.addClass('open');
        }
    });


}

function addCondition(thisItem) {
    var newCondition = $('#route_element_location').clone().removeAttr('id');
    var parent = $(thisItem).parents(".route_element");



    newCondition.children('.selectpicker').selectpicker({
        size: false
    });

    newCondition.find('.multiselect').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 250
    });

    newCondition.find('.btn.dropdown-toggle').click(function() {
        console.log("from button handler!");
        var $parent = $(this).parents('div.btn-group');
        var $isOpen = $parent.hasClass('open');

        if($isOpen) {
            $parent.removeClass('open');
        } else {
            $parent.addClass('open');
        }
    });

    newCondition.css("display", "block");
    newCondition.insertAfter(parent).hide().slideDown('slow');
}

function deleteCondition(deleteButton) {

    $deleteMe = $(deleteButton).parents(".route_element");
    console.log("Deleting" + $deleteMe.className);

    $deleteMe.slideUp('slow', function (){
        $deleteMe.remove();
    });
}

// TODO change the text in the variables box.
function changeCondition(thisItem) {
    console.log("Changing to " + $(thisItem).val());
    var $parent = $(thisItem).parents(".route_element");
    var newElement;
    //var item = thisItem;
    var value = $(thisItem).val();

    var $locationVars = { "Origin":"o_code", "Destination":"d_code"};
    var $timeVars = { "Delay":"delay", "Duration":"duration", "Takeoff":"takeoff", "Landing":"landing" };
    var $weatherVars = { "Origin Temp":"o_current", "Destination Temp":"d_current" , "Difference":"difference" };
    var $itinVars = { "Date":"flight_date", "Airline Code":"airline_code", "Flight Number":"flight_number",
        "Origin Code":"o_code", "Origin Gate":"o_gate", "Scheduled Takeoff":"o_scheduled",
        "Origin Terminal":"o_terminal", "Destination Code":"d_code", "Scheduled Landing":"d_scheduled" };
    var $gateVars = { "Old Gate":"g_old", "New Gate":"g_new" };

    var select = $('#add_variable');
    select.find('option').remove();

    if(value == 'location') {
        newElement = $('#route_element_location').clone().css('display','block');
        newElement.removeAttr('id');

        newElement.find('.multiselect').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 250
        });

        $.each($locationVars, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });

    } else if(value == 'time') {
        newElement = $('#route_element_time').clone().css('display','block');
        newElement.removeAttr('id');

        newElement.find('select[name=time_condition]').attr('id', 'time_selectpicker');

        $.each($timeVars, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });
    } else if (value == 'weather') {
        newElement = $('#route_element_weather').clone().css('display','block');
        newElement.removeAttr('id');

        $.each($weatherVars, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });
    } else if (value == 'before_takeoff') {
        newElement = $('#route_element_before_takeoff').clone().css('display', 'block');
        newElement.removeAttr('id');

        $.each($itinVars, function(key, value) {
            select.append($("<option></option>")
                    .attr("value", value).text(key));
        });
    } else if (value == 'itinerary' || value == 'gate') {
        newElement = $('#route_element_single_condition').clone().css('display', 'block');
        newElement.children('.selectpicker').val(value);
        newElement.removeAttr('id');

        if(value == 'itinerary') {
            $.each($itinVars, function(key, value) {
                select.append($("<option></option>")
                        .attr("value", value).text(key));
            });
        } else if (value == 'gate') {
            $.each($gateVars, function(key, value) {
                select.append($("<option></option>")
                        .attr("value", value).text(key));
            });
        }
    }

    select.selectpicker('refresh');

    newElement.children('.selectpicker').selectpicker({
        size:false
    });

    newElement.find('.btn.dropdown-toggle').click(function() {
        console.log("from button handler!");
        var $parent = $(this).parents('div.btn-group');
        var $isOpen = $parent.hasClass('open');

        if($isOpen) {
            $parent.removeClass('open');
        } else {
            $parent.addClass('open');
        }
    });

    newElement.insertBefore($parent).hide().slideDown('slow');

    $parent.slideUp('slow', function() {
        $parent.remove();
    });
}

function appendLast() {
    var $newText = $('#notif_text').val();
    var $newUsers = "";
    $("input[name='user[]']:checked").each(function () {
        $newUsers += $(this).attr('id') + "<br>";
    });

    // TODO make this pretty
    $('#sentItems').append("<p><b>" + $newText + "</b> sent to <b>" + $newUsers + "</b></p>");
    $('#send')[0].reset();
}

function checkTimes(button) {
    var condition = $(button).val();
    console.log("changing to " + condition);

    if(condition == "always") {
        console.log("ALWAYS!!");
        $('#dates').slideUp('slow', function () {
            $('#dates').css('display', 'none');
        });

        $('#btn-always').addClass('btn-primary');
        $('#btn-specify').removeClass('btn-primary');

    } else if (condition == "specify") {
        console.log("SPECIFY!");

        $('#dates').slideDown('slow', function () {
            $('#dates').css('display', 'block');
        });

        $('#btn-specify').addClass('btn-primary');
        $('#btn-always').removeClass('btn-primary');

    }
}

function showAvailableVars(select) {
    console.log("Variable selected! " + $(select).val());

    // TODO this needs to add the variable to the text box.
    var $form = $(select).parents('#new_route_controls');

    var $textBox = $($form).find('#notif_subtitle');

    console.log("textBox value= " + $textBox.val());
    $textBox.append("[[" + $(select).val() + "]]");



    var $parent = $form.find('div.btn-group.show-tick');

    var $isOpen = $parent.hasClass('open');

    if($isOpen) {
        $parent.removeClass('open');
    } else {
        $parent.addClass('open');
    }

    $(select).children('option').removeAttr('selected');

    $('#add_variable').selectpicker('refresh');
}

</script>


<div class="navbar navbar-inverse navbar-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header-inverse">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#">Notification Console</a>
            </div>
            <div class="collapse nav-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="./stored.jsp">Stored Rules</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div> <!-- /.navbar -->

<div class="container-narrow" id="parent">
    <div class="container-fluid">


        <form id="send" class="form sp">
            <label for="notif_text">Notification Text</label>
            <input id="notif_text" name="notif_text" class="text input-lg form-control" placeholder="Enter some text to send.">
            <input id="send_btn" class="btn btn-primary" value="Send Notification!"  name="send_notification">
            <br>
            <%
                Map theMap = User.readDB();
                ArrayList<String> users = (ArrayList<String>) theMap.get("names");
                ArrayList<String> ids = (ArrayList <String>) theMap.get("ids");

                for(int i =0; i < users.size(); i ++)
                { %>
            <input type="checkbox" name="user[]" id="<%=users.get(i)%>" value="<%=ids.get(i)%>"> <%=users.get(i)%><br>
            <% }
            %>

        </form>

        <p>
            <button id="new_route" onclick="showConditions()" class="btn btn-large btn-primary">Create New Notification  <i class="icon-plus-sign"></i></button>
        </p>

        <div id="new_route_element_select" class="new_route_elements">
            <form id="create_new_route" class="create_new_route row-fluid">
                <div class="span6" id="if-container">


                </div>

                <div class="span6">
                    <div id="new_route_controls" class="route_element new_route_controls" style="display:none;">

                        <div>
                            <div class="preposition-label">Then Send</div>
                            <select id="then_send" name="title" class="selectpicker">
                                <option value="Flight Status Notification">Flight Status Notification</option>
                                <option value="Weather Notification">Weather Notification</option>
                                <option value="Promotion Notification">Promotion Notification</option>
                                <option value="Check In Notification">Check In Notification</option>
                            </select>

                            <div class="preposition-label">Via</div>

                            <select id="notify" name="notify" class="selectpicker">
                                <option value="android">Android Push Notification</option>
                                <option value="apple">Apple Push Notification</option>
                                <option value="sms">SMS</option>
                                <option value="email">Email</option>
                            </select>
                            <div>
                                <div class="preposition-label">Notification Text</div>
                                <textarea id="notif_subtitle" name="notification_subtitle" class="form-control"></textarea>
                                <select multiple data-title="Add a variable" id="add_variable" class="selectpicker" onchange="showAvailableVars(this)" data-icon="icon-anchor">
                                    <option value="o_code">Origin</option>
                                    <option value="d_code">Destination</option>
                                </select>
                            </div>

                            <div class="btn-group start-end-btns">
                                <button type="button" id="btn-always" value="always" class="btn btn-primary" onclick="checkTimes(this)">Always</button>
                                <button type="button" id="btn-specify" value="specify" class="btn" onclick="checkTimes(this)">Specify When</button>
                            </div>
                            <div id="dates" style="display:none;">
                                <div class="preposition-label">Start Date</div>
                                <div class="controls input-append date form_datetime" data-link-field="dtp_input1" data-date-format="yyyy-mm-dd hh:ii:ss">
                                    <input id="cal_start" name="start_date" type="text" class="input-large" value="" size="16"/>
                            <span class="add-on">
                                <i class="icon-remove"></i>
                            </span>
                            <span class="add-on">
                                <i class="icon-th"></i>
                            </span>
                                </div>

                                <div class="preposition-label">End Date</div>
                                <div class="controls input-append date form_datetime" data-link-field="dtp_input1" data-date-format="yyyy-mm-dd hh:ii:ss">
                                    <input id="cal_end" name="stop_date" type="text" class="input-large" value="" size="16"/>
                            <span class="add-on">
                                <i class="icon-remove"></i>
                            </span>
                            <span class="add-on">
                                <i class="icon-th"></i>
                            </span>
                                </div>
                            </div>

                            <div>
                                <button id="new_route_submit" class="btn btn-lg btn-success">Start Rule  <i class="icon-play"></i></button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="sentItems">

</div>


<!-- Hidden classes. Clone these in JQuery to set up the interface. -->
<div class="route_element route_element_location" id="route_element_location" style="display:none;">
    <div class="preposition-label">If</div>
    <select class="selectpicker" id="type" name="type" onchange="changeCondition(this)">
        <option value="location" selected="selected">Location</option>
        <option value="time">Time</option>
        <option value="weather">Weather</option>
        <option value="before_takeoff">Time Before Takeoff</option>
        <option value="itinerary">Itinerary Changes</option>
        <option value="gate">Gate Changes</option>
    </select>

    <div class="preposition-label">at</div>
    <select class="selectpicker" name="location_loc" >
        <option value="o">Origin</option>
        <option value="d">Destination</option>
    </select>

    <div class="preposition-label">is</div>

    <select name="airport" id="airports" class="multiselect input-block-level" multiple="multiple" >
        <option value="DFW">Dallas</option>
        <option value="AUH">Abu Dhabi</option>
        <option value="JFK">New York</option>
        <option value="LAX">Los Angeles</option>
    </select>

    <div class="modify-conditions">
        <button class="add_condition btn btn-success" onclick="addCondition(this)" type="button"><i class="icon-plus-sign"></i></button>
        <button class="delete_condition btn btn-danger" onclick="deleteCondition(this)" type="button"><i class="icon-minus-sign"></i></button>
    </div>

    <br style="clear:both"/>
</div>

<div class="route_element route_element_time" id="route_element_time" style="display:none">

    <div class="preposition-label">If</div>
    <select class="selectpicker" name="type" onchange="changeCondition(this)">
        <option value="location">Location</option>
        <option value="time" selected="selected">Time</option>
        <option value="weather">Weather</option>
        <option value="before_takeoff">Time Before Takeoff</option>
        <option value="itinerary">Itinerary Changes</option>
        <option value="gate">Gate Changes</option>
    </select>

    <div class="preposition-label">of</div>

    <select class="time_anchor selectpicker" name="time_anchor" onchange="changeTimeLayout(this)">
        <option value="delay">Delay</option>
        <option value="duration">Duration</option>
        <option value="o_scheduled">Takeoff</option>
        <option value="d_scheduled">Landing</option>
    </select>

    <div class="preposition-label">is</div>

    <select class="time_condition selectpicker" name="time_condition">
        <option value="<">Less than</option>
        <option value=">">Greater than</option>
    </select>

    <input class="time_value form-control" type="number" name="time_value" placeholder="Minutes">


    <div class="modify-conditions">
        <button class="add_condition btn btn-success" onclick="addCondition(this)" type="button"><i class="icon-plus-sign"></i></button>
        <button class="delete_condition btn btn-danger" onclick="deleteCondition(this)" type="button"><i class="icon-minus-sign"></i></button>
    </div>
    <br style="clear:both;"/>

</div>

<div class="route_element route_element_weather" id="route_element_weather" style="display:none">

    <div class="preposition-label">If</div>
    <select class="selectpicker" name="type" onchange="changeCondition(this)">
        <option value="location">Location</option>
        <option value="time">Time</option>
        <option value="weather" selected="selected">Weather</option>
        <option value="before_takeoff">Time Before Takeoff</option>
        <option value="itinerary">Itinerary Changes</option>
        <option value="gate">Gate Changes</option>
    </select>

    <div class="preposition-label">at</div>
    <select class="selectpicker" name="weather_loc">
        <option value="o">Origin</option>
        <option value="d">Destination</option>
        <option value="diff">Difference</option>
    </select>

    <div class="preposition-label">is</div>
    <select class="selectpicker" name="weather_condition" style="margin-right:10px;">
        <option value="<">Less Than</option>
        <option value=">">Greater Than</option>
    </select>
    <div class="input-group">
        <input type="text" class="degrees form-control" name="degrees" placeholder="Degrees">
        <span class="input-group-addon">Celsius</span>
    </div>

    <div class="modify-conditions">
        <button class="add_condition btn btn-success" onclick="addCondition(this)" type="button"><i class="icon-plus-sign"></i></button>
        <button class="delete_condition btn btn-danger" onclick="deleteCondition(this)" type="button"><i class="icon-minus-sign"></i></button>
    </div>
    <br style="clear:both"/>
</div>

<div class="route_element route_element_before_takeoff" id="route_element_before_takeoff" style="display:none">

    <div class="preposition-label">If</div>
    <select class="selectpicker" name="type" onchange="changeCondition(this)">
        <option value="location">Location</option>
        <option value="time">Time</option>
        <option value="weather">Weather</option>
        <option value="before_takeoff" selected="selected">Time Before Takeoff</option>
        <option value="itinerary">Itinerary Changes</option>
        <option value="gate">Gate Changes</option>
    </select>

    <div class="preposition-label">is</div>

    <select class="time_condition selectpicker" name="time_before_takeoff_condition">
        <option value="<">Less than</option>
        <option value=">">Greater than</option>
    </select>

    <input class="time_value form-control" type="number" name="before_value" placeholder="Hours">


    <div class="modify-conditions">
        <button class="add_condition btn btn-success" onclick="addCondition(this)" type="button"><i class="icon-plus-sign"></i></button>
        <button class="delete_condition btn btn-danger" onclick="deleteCondition(this)" type="button"><i class="icon-minus-sign"></i></button>
    </div>
    <br style="clear:both;"/>

</div>

<div class="route_element route_element_single_condition" id="route_element_single_condition" style="display:none">

    <div class="preposition-label">If</div>
    <select class="selectpicker" name="type" onchange="changeCondition(this)">
        <option value="location">Location</option>
        <option value="time">Time</option>
        <option value="weather">Weather</option>
        <option value="before_takeoff">Time Before Takeoff</option>
        <option value="itinerary" selected="selected">Itinerary Changes</option>
        <option value="gate">Gate Changes</option>
    </select>

    <!-- <div class="preposition-label">is</div>

     <select class="time_condition selectpicker" name="time_before_takeoff_condition">
         <option value="<">Less than</option>
         <option value=">">Greater than</option>
     </select>

     <input class="time_value form-control" type="number" name="before_value" placeholder="Hours">  -->


    <div class="modify-conditions">
        <button class="add_condition btn btn-success" onclick="addCondition(this)" type="button"><i class="icon-plus-sign"></i></button>
        <button class="delete_condition btn btn-danger" onclick="deleteCondition(this)" type="button"><i class="icon-minus-sign"></i></button>
    </div>
    <br style="clear:both;"/>

</div>

</body>
</html>