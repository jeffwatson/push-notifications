<%@ page import="java.util.List" %>
<%@ page import="com.sabre.PushNotificationsWeb.Route" %>
<%--
  Created by IntelliJ IDEA.
  User: Jeff Watson <Jeff.Watson@sabre.com>
  Date: 9/9/13
  Time: 3:35 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Push Notifications-Stored Rules</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <script src="js/bootstrap.min.js"></script>

    <!-- CSS -->
    <link href="css/main.css" rel="stylesheet" media="screen">
</head>
<body>
<div class="navbar navbar-inverse navbar-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header-inverse">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#">Notification Console</a>
            </div>
            <div class="collapse nav-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="./index.jsp">Home</a></li>
                    <li class="active"><a href="./stored.jsp">Stored Rules</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div> <!-- /.navbar -->

<div class="container-narrow">


    id | conditions | start | stop <br><br>
    <%  List<Route> routes = Route.getRoutes();
        for(Route route: routes) { %>

    <div><%=route.getId()%>       |
        <%=route.getValue()%>    |
        <%=route.getStartDate()%> |
        <%=route.getStopDate()%>   |
    </div>

    <% }%>

</div>

</body>
</html>